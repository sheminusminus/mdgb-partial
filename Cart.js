import React from 'react';
import { render } from 'react-dom';
import firebase from 'firebase';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

const config = {
    apiKey: "AIzaSyA4qAa1Lu0HfGTtXvNtwDBC4YXv9EPDtiY",
    authDomain: "egbdev-38305.firebaseapp.com",
    databaseURL: "https://egbdev-38305.firebaseio.com",
    storageBucket: "egbdev-38305.appspot.com",
    messagingSenderId: "721932278461"
};

firebase.initializeApp(config);

// the cart app
import Cart from './cart-app/index';

// for better control over test and live mode
const ENV = window.ENV;
const catalog = require('./json/catalog.json')[ENV];
const skuList = require('./json/skus.json')[ENV];

render(
    <Cart skuList={skuList} catalog={catalog} />,
    document.querySelector('#react')
);

