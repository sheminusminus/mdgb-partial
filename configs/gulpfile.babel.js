'use strict';

import plugins  from 'gulp-load-plugins';
import yargs    from 'yargs';
import browser  from 'browser-sync';
import gulp     from 'gulp';
import panini   from 'panini';
import rimraf   from 'rimraf';
import sherpa   from 'style-sherpa';
import yaml     from 'js-yaml';
import fs       from 'fs';

const $ = plugins();

const PRODUCTION = !!(yargs.argv.production);

const { COMPATIBILITY, PORT, UNCSS_OPTIONS, PATHS } = loadConfig();

function loadConfig() {
  let ymlFile = fs.readFileSync('config.yml', 'utf8');
  return yaml.load(ymlFile);
}

// build the "dist" folder
gulp.task('build',
 gulp.series(clean, gulp.parallel(pages, sass, javascript, images, copy, homepage)));

// build the site, run the server, and watch for file changes
gulp.task('default',
  gulp.series('build', server, watch));

function clean(done) {
  rimraf(PATHS.dist, done);
}

// skips over the "img", "js", and "scss" folders, which are parsed separately
function copy() {
  return gulp.src(PATHS.assets)
    .pipe(gulp.dest(PATHS.dist + '/assets'));
}

// copy page templates into finished html files
function pages() {
  return gulp.src('src/pages/**/*.{php,html,hbs,handlebars}')
    .pipe(panini({
      root: 'src/pages/',
      layouts: 'src/layouts/',
      partials: 'src/partials/',
      data: 'src/data/',
      helpers: 'src/helpers/'
    }))
    .pipe(gulp.dest(PATHS.dist));
}

// load updated HTML templates and partials into panini
function resetPages(done) {
  panini.refresh();
  done();
}

// style guide
function styleGuide(done) {
  sherpa('src/styleguide/index.md', {
    output: PATHS.dist + '/styleguide.html',
    template: 'src/styleguide/template.html'
  }, done);
}

function sass() {
  return gulp.src('src/assets/scss/app.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      includePaths: PATHS.sass
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: COMPATIBILITY
    }))
    //.pipe($.if(PRODUCTION, $.uncss(UNCSS_OPTIONS)))
    .pipe($.if(PRODUCTION, $.cssnano()))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + '/assets/css'))
    .pipe(browser.reload({ stream: true }));
}

function javascript() {
  return gulp.src(PATHS.javascript)
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.concat('app.js'))
    .pipe($.if(PRODUCTION, $.uglify()
      .on('error', e => { console.log(e); })
    ))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + '/assets/js'));
}

// copy images to the "dist" folder
function images() {
  return gulp.src('src/assets/img/**/*')
    .pipe(gulp.dest(PATHS.dist + '/assets/img'));
}

// copy landing page css styles to dist
function homepage() {
  return gulp.src('src/assets/css/home.css')
    .pipe(gulp.dest(PATHS.dist + '/assets/home.css'));
}

// start a server with browsersync (dev only)
function server(done) {
  browser.init({
    proxy: "http://localhost:3000",
    port: PORT
  });
  done();
}

// browsersync reload
function reload(done) {
  browser.reload();
  done();
}

// watch for changes to static assets, pages, sass, and js
function watch() {
	gulp.watch('src/assets/css/home.css').on('change', gulp.series(copy, browser.reload));
//  gulp.watch(PATHS.assets, copy);
//     gulp.watch('src/pages/**/*.php').on('change', gulp.series(pages, browser.reload));
  gulp.watch('src/pages/**/*.html').on('change', gulp.series(pages, browser.reload));
  gulp.watch('src/{layouts,partials}/**/*.html').on('change', gulp.series(resetPages, pages, browser.reload));
  gulp.watch('src/assets/scss/**/*.scss', sass);
  gulp.watch('src/assets/js/**/*.js').on('change', gulp.series(javascript, browser.reload));
  gulp.watch('src/assets/img/**/*').on('change', gulp.series(images, browser.reload));
  gulp.watch('src/assets/css/*.css').on('change', browser.reload);
}
