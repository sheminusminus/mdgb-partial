import userPayloadReducer from './user';
import cartReducer from './cart';
import eventDataReducer from './event';
import bookDataReducer from './book';
import { basicInfoReducer, defaultsReducer } from './account';
import tempEventDataReducer from './tmp-event';
import addonCartReducer from './addon-cart';

import { 
	// ui based
	SWITCH_ACTIVE_TAB, TOGGLE_MENU, SWITCH_DESIGN_TAB,
	// database callbacks
	INITIAL_USER_DATA, NEW_ACCOUNT_DATA, BASIC_INFO_CHANGED, 
	BOOK_VALUES_CHANGED, EVENT_VALUES_CHANGED, DEFAULTS_CHANGED,
	SHIPPING_CHANGED, SHIPPING_UDPATED, REFRESH_EVENT_DATA,
	// errors or callouts
	DISPLAY_USER_ALERT, HIDE_USER_ALERT,
	// personalizations
	COVER_TITLE_POSITIONED, COVER_TEXT_CHANGED, COVER_IMAGE_SAVED,
	ACTIVE_EVENT_MODIFIED, ACTIVE_BOOK_MODIFIED, CHOOSE_ACTIVE_EVENT,
	PHOTOSET_MODIFIED,
	TEMP_EVENT_MODIFIED,
	// addons
	ADDON_CART_EDIT, ADDON_CART_UPDATED
} from '../action-types';

var historyLocation = function(state, action) {
	state.historyLocation = action.payload;
	return state;
}

var errorMessageReducer = function(state, action) {
	state.error = action.payload;
	return state;
}

var switchActiveEvent = function(newActiveEvent) {
	var activeEvent = eventDataReducer(newActiveEvent);
	return activeEvent;
}

var setInitialActiveEvent = function(userEvents) {
	let key = Object.keys(userEvents)[0]
	let activeEvent = eventDataReducer(userEvents[key]);
	return activeEvent;
}

var setInitialActiveBook = function(userEvents, userBooks) {
	let key = Object.keys(userEvents)[0];
	let activeBook = bookDataReducer(userBooks[key], key);
	return activeBook;
}

var getUpdatedUserData = function(uid, user) {
	var userData = userPayloadReducer(uid, user);
	return userData;
}

var updateAccountData = function(oldData, payload) {
	var userData = accountDataReducer(oldData, payload);
	return userData;
}

var updateCartData = function(withCartItems) {
	var userCart = cartReducer(withCartItems);
	return userCart;
}

const getDeepEqualObjects = function(stateData, uid, data) {
	var newData = (JSON.parse(JSON.stringify(stateData)));
	newData.userID = uid;
	newData.defaults = data.defaults ? data.defaults : {};
	newData.basicInfo = data.basicInfo ? data.basicInfo : {};	
	newData.orders = data.orders ? data.orders : {};
	newData.cart = data.cart ? data.cart : {};
	newData.web_activity = data.web_activity ? data.web_activity : stateData.web_activity;
	return newData;
};

var shallowBooks = function(bks) {
	if (bks === undefined) { return {}; }
	var books = {};
	const keys = Object.keys(bks);
	for (var i = 0; i < keys.length; i++) {
		var eid = keys[i];
		books[eid] = {
			eventID: eid,
			titlePosition: bks[eid].titlePosition ? bks[eid].titlePosition : 'middle', 
			coverImage: bks[eid].coverImage ? bks[eid].coverImage : 'assets/stock_thumbs/default.png',
			urls: bks[eid].urls ? bks[eid].urls : [],
			leftOut: bks[eid].leftOut ? bks[eid].leftOut : [],
			coverText: bks[eid].coverText ? bks[eid].coverText : {line1: '', line2: '', line3: ''}
		};
	}
	return books;
}

var shallowEvents = function(obj) {
	if (obj === undefined) { return {}; }
	var evts = {};
	const keys = Object.keys(obj);
	for (var i = 0; i < keys.length; i++) {
		var eid = keys[i];
		evts[eid] = { 
			eventName: obj[eid].eventName ?  obj[eid].eventName : '', 
			eventDate: obj[eid].eventDate ? obj[eid].eventDate : new Date(),
			dateString: obj[eid].dateString ? obj[eid].dateString : (new Date()).toLocaleDateString(),
			photos: obj[eid].photos ? obj[eid].photos : 0,
			orderID: obj[eid].orderID ? obj[eid].orderID : false,
			eventID: obj[eid].eventID,
			finalized: obj[eid].finalized === undefined ?  false : obj[eid].finalized,
			urls: obj[eid].urls ? obj[eid].urls : {},
			tiny: obj[eid].tiny ? obj[eid].tiny : {}, 
			thumb: obj[eid].thumb ? obj[eid].thumb : {}
		};
	}
	return evts;
}

var dbTypesReducer = function(state, action) {
	switch(action.type) {
		case INITIAL_USER_DATA:
			var newState = getDeepEqualObjects(state, action.payload.userID, action.payload.userData);
			newState.books = shallowBooks(action.payload.userData.books);
			newState.events = shallowEvents(action.payload.userData.events);
			newState.activeEvent = setInitialActiveEvent(newState.events);
			newState.cart = updateCartData(action.payload.userData.cart);
			newState.activeBook = setInitialActiveBook(newState.events, newState.books);
			newState.newUser = action.payload.isNew;
			newState.tempEvent = tempEventDataReducer(state, action.payload.userData.tmp_event);
			return newState;
		case NEW_ACCOUNT_DATA:
			var newState = getDeepEqualObjects(state, action.payload.userID, action.payload.userData);
			newState.books = shallowBooks(action.payload.userData.books);
			newState.events = shallowEvents(action.payload.userData.events);
			newState.activeEvent = setInitialActiveEvent(newState.events);
			newState.cart = updateCartData(action.payload.userData.cart);
			newState.activeBook = setInitialActiveBook(newState.events, newState.books);
			return newState;
		case BASIC_INFO_CHANGED:
			var newState = (JSON.parse(JSON.stringify(state)));
			newState.basicInfo = action.payload;
			newState.saving = false;
			return newState;
		case DEFAULTS_CHANGED:
			var newState = (JSON.parse(JSON.stringify(state)));
			newState.defaults = action.payload;
			newState.saving = false;
			return newState;
		case ACTIVE_EVENT_MODIFIED:
			var newState = (JSON.parse(JSON.stringify(state)));
			newState.activeEvent = eventDataReducer(action.payload);
			return newState;
		case REFRESH_EVENT_DATA:
			var newState = (JSON.parse(JSON.stringify(state)));
			var eventID = action.payload.eid;
			var eventList = action.payload.list;
			newState.activeEvent = eventDataReducer(eventList[eventID]);
			return newState;
		default:
			return state;
	}
}

var uiTypesReducer = function(state, action) {
	var newState = (JSON.parse(JSON.stringify(state)));
	switch(action.type) {
		case SWITCH_ACTIVE_TAB:
			newState.activeTabIdx = action.payload;
			return newState;
		case TOGGLE_MENU:
			newState.menuOpen = action.payload.shouldClose ? false : !state.menuOpen;
			newState.activeTabIdx = action.payload.tabIdx;
			return newState;
		case SWITCH_DESIGN_TAB:
			newState.designTab = action.payload;
			return newState;
		default:
			return state;
	}
}

var savingIndicatorReducer = function(state, action) {
	let newState = (JSON.parse(JSON.stringify(state)));
	switch(action.type) {
		case SHIPPING_CHANGED:
			newState.saving = action.payload;
			console.log('saving --->', newState);
			return newState;
		default:
			return newState;
	}
}

var bookDesignReducer = function(state, action) {
	switch(action.type) {
		case ACTIVE_BOOK_MODIFIED:
			let newState = (JSON.parse(JSON.stringify(state)));
			newState.activeBook = bookDataReducer(action.payload.book, action.payload.eid);
			return newState;
		default:
			return state;
	}
}

var activeReducer = function(state, action) {
	switch(action.type) {
		case CHOOSE_ACTIVE_EVENT:
			var newState = (JSON.parse(JSON.stringify(state)));
			newState.activeEvent = eventDataReducer(action.payload);
			return newState;
		default:
			return state;
	}
}

var tmpEventReducer = function(state, action) {
	var newState = (JSON.parse(JSON.stringify(state)));
	switch (action.type) {
		case TEMP_EVENT_MODIFIED:
			newState.tempEvent = tempEventDataReducer(action.payload);
			return newState;
		default:
			return state;
	}
	
}

var addonReducer = function(state, action) {
	var newState = (JSON.parse(JSON.stringify(state)));
	newState.addonCart = addonCartReducer(action.payload);
	return newState;
}


var rootReducer = function(state, action) {
	if (!state) { state = 0 };
	switch(action.type) {
		case INITIAL_USER_DATA:
			return dbTypesReducer(state, action);
		case NEW_ACCOUNT_DATA:
			return dbTypesReducer(state, action);
		case BASIC_INFO_CHANGED:
			return dbTypesReducer(state, action);
		case DEFAULTS_CHANGED:
			return dbTypesReducer(state, action);
		case REFRESH_EVENT_DATA:
			return dbTypesReducer(state, action);
		case SWITCH_ACTIVE_TAB:
			return uiTypesReducer(state, action);
		case TOGGLE_MENU:
			return uiTypesReducer(state, action);
		case SWITCH_DESIGN_TAB:
			return uiTypesReducer(state, action);
		case DISPLAY_USER_ALERT:
			return errorMessageReducer(state, action);
		case HIDE_USER_ALERT:
			return errorMessageReducer(state, action)
		case COVER_TITLE_POSITIONED:
			return bookDesignReducer(state, action);
		case COVER_TEXT_CHANGED:
			return state;
		case COVER_IMAGE_SAVED:
			return state;
		case DEFAULTS_CHANGED:
			return state;
		case SHIPPING_CHANGED:
			return savingIndicatorReducer(state, action);
		case SHIPPING_UDPATED:
			return savingIndicatorReducer(state, action);
		case CHOOSE_ACTIVE_EVENT:
			return activeReducer(state, action);
		case ACTIVE_EVENT_MODIFIED:
			return dbTypesReducer(state, action);
		case ACTIVE_BOOK_MODIFIED:
			return bookDesignReducer(state, action);
		case PHOTOSET_MODIFIED:
			return state;
		case TEMP_EVENT_MODIFIED:
			return tmpEventReducer(state, action);
		case ADDON_CART_UPDATED:
			return addonReducer(state, action);
		default:
			return state;
	}
}

export default rootReducer;