import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase';
import { ajax } from 'jquery';
import createHistory from 'history/createBrowserHistory';

var db, storage, admin, BASE_URL;

var history = createHistory({
    basename: '/admin'
});


// for better control over test and live mode
const ENV = window.ENV;
const catalog = require('../json/catalog.json')[ENV];
const skus = require('../json/skus.json')[ENV];
const bookSKU = skus['book'];
const photoSKU = skus['hPhoto'];
const hardSKU = skus['hard'];
const softSKU = skus['soft'];

// logs if test mode: window.devMsg(compNameStr, [reqPropStr], exampleUseStr)
import DevInfo from './shared/DevInfo';

// firebase writeable user-data model
import User from './objects/user';

// action types and creator function
import act from './create-actions';
import {
	UPDATE_OLD_ACCOUNT,
	FORWARD, BACK, NAV,
	DISPLAY_USER_ALERT, HIDE_USER_ALERT, 
	SWITCH_ACTIVE_TAB, TOGGLE_MENU, SWITCH_DESIGN_TAB, USER_SIGNUP_EMAIL,
	NEW_ACCOUNT_DATA, INITIAL_USER_DATA, BASIC_INFO_CHANGED,
	FIRST_CHANGED, LAST_CHANGED, PHONE_CHANGED, SHIPPING_CHANGED,
	ACTIVE_EVENT_MODIFIED, ACTIVE_BOOK_MODIFIED, CHOOSE_ACTIVE_EVENT,
	DEFAULTS_CHANGED, COVER_TITLE_POSITIONED, COVER_TEXT_CHANGED, COVER_IMAGE_ADDED,
	COVER_IMAGE_UPLOADED, SET_STOCK_COVER, SET_TEMP_COVER,
	EVENT_NAME_EDITED, EVENT_DATE_EDITED, EVENT_PHOTO_CHANGED, COVER_TEXT_EDITED,
	CREATE_INDEX_CHANGE, CART_INDEX_CHANGE, CART_ACTION, DATE_FORMAT_CHOSEN, 
	PHOTOSET_ADDITION, SET_COVER_COLORS, REFRESH_EVENT_DATA, SET_BOOK_URLS,
	PHOTOSET_SUBMISSION,
	TEMP_NAME_EDITED, TEMP_DATE_EDITED, TEMP_IMAGE_EDITED, TEMP_TEXT_EDITED, SET_TEMP_STOCK, TEMP_EVENT_MODIFIED,
	ADDON_CART_EDIT, ADDON_CART_UPDATED
} from './action-types';

// reducer
import rootReducer from './reducers/root';

// constants
import { Priority } from './constants';

// components
import EventMarquee from './event-marquee/EventMarquee';
import Menu from './menu/Menu';
import WelcomeUser from './welcome-user/WelcomeUser';
import MainBody from './containers/MainBody';
import NavButton from './buttons/NavButton';
import UserAlert from './shared/UserAlert';
import AuthWrapper from './data-wrappers/AuthWrapper';

//=======================
// view
//=======================

class App extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			userID: '',
			basicInfo: { email: '', firstName: '', lastName: '', phone: '', customerID: '', customerID: '' },
			defaults: { shipping: { line1: '', line2: '', city: '', state: '', postal: '' } },
			events: {},
			orders: {},
			books: {},
			cart: {},
			addonCart: {},
			activeEvent: {},
			activeBook: {},
			activeTabIdx: 0,
			menuOpen: false,
			newUser: false,
			menuOptions: ['Create an Event', 'Manage an Event', 'App Tips', 'Account', 'Support', 'Logout'],
			error: null,
			web_activity: {designTab: 0, creationIndex: 0, cartIndex: 0, cartActions: []},
			saving: false,
			tempEvent: {}
		};
		
		// firebase managers
		db = firebase.database();
		storage = firebase.storage();
		admin = firebase.auth();
		
		// state machine
		this.restate = this.restate.bind(this);
		
		// callback functions
		this.handleLogin = this.handleLogin.bind(this);
		this.handleSignUp = this.handleSignUp.bind(this);
		this.handleLogout = this.handleLogout.bind(this);
		this.handleMenuClick = this.handleMenuClick.bind(this);
		this.handleDateFormatSelect = this.handleDateFormatSelect.bind(this);
		this.handleName = this.handleName.bind(this);
		this.handleFirst = this.handleFirst.bind(this);
		this.handleLast = this.handleLast.bind(this);
		this.handlePhone = this.handlePhone.bind(this);
		this.handleDesignTab = this.handleDesignTab.bind(this);
		this.handleCreateStep = this.handleCreateStep.bind(this);
		this.chooseArea = this.chooseArea.bind(this);
		this.handleDateChange = this.handleDateChange.bind(this);
		this.handleShippingChange = this.handleShippingChange.bind(this);
		this.createNewUser = this.createNewUser.bind(this);
		this.selectActiveEvent = this.selectActiveEvent.bind(this);
		this.toggleMenu = this.toggleMenu.bind(this);
		this.setError = this.setError.bind(this);
		this.hideError = this.hideError.bind(this);
		this.handleTitles = this.handleTitles.bind(this);
		this.handleCoverColors = this.handleCoverColors.bind(this);
		this.toggleSelect = this.toggleSelect.bind(this);
		this.handleStockChange = this.handleStockChange.bind(this);
		this.startAddonOrder = this.startAddonOrder.bind(this);
		this.startSubmission = this.startSubmission.bind(this);
		this.setBookPhotoLinks = this.setBookPhotoLinks.bind(this);
		this.handleAddon = this.handleAddon.bind(this);
		
		// database + db-listener callbacks
		this.fetchInitialUserData = this.fetchInitialUserData.bind(this);
		this.listenToBasicInfo = this.listenToBasicInfo.bind(this);
		this.listenToBooks = this.listenToBooks.bind(this);
		this.listenToEvents = this.listenToEvents.bind(this);
		this.listenToDefaults = this.listenToDefaults.bind(this);
		this.writeToDatabase = this.writeToDatabase.bind(this);
		this.writeToStorage = this.writeToStorage.bind(this);
		this.silenceOldListeners = this.silenceOldListeners.bind(this);
		this.pingEventID = this.pingEventID.bind(this);
		this.newEvent = this.newEvent.bind(this);
		this.extractFromDeprecatedData = this.extractFromDeprecatedData.bind(this);
		this.uploadAdditionAsString = this.uploadAdditionAsString.bind(this);
		this.handleTempName = this.handleTempName.bind(this);
		this.handleTempDate = this.handleTempDate.bind(this);
		this.handleTempTitles = this.handleTempTitles.bind(this);
		this.handleTempStockChange = this.handleTempStockChange.bind(this);
		this.handleTempStockChange = this.handleTempStockChange.bind(this);
		this.handleTempImageUpload = this.handleTempImageUpload.bind(this);
		this.saveAddonToCart = this.saveAddonToCart.bind(this);
		this.handleSelectArea = this.handleSelectArea.bind(this);
		
		// global error handling
		window.setError = this.setError.bind(this);
	}
	
	//=======================
	// new state machine
	//=======================
	
	restate(nextState, firstSet, resetActives) {
		this.setState(
			nextState,
			function() {
				console.info('---> new state:', this.state);
				if (firstSet) {
					this.listenToActiveOnly(nextState.userID, nextState.activeEvent.eventID);
					this.listenToBasicInfo(nextState.userID);
					this.listenToDefaults(nextState.userID);
					this.listenToTmp(nextState.userID);
				}
				else if (resetActives) {
					this.listenToActiveOnly(nextState.userID, nextState.activeEvent.eventID);
				}
			}
		);
	}
	
	//=======================
	// lifecycle / setup / cleanup
	//=======================

	componentDidMount(){
		var context = this;
		this.setAuthHandler(null);
		this.unlisten = history.listen(function(location, action) {
			context.handleMenuClick(location.state.tabIdx, true);		
        });
	}
	
	componentWillUnmount(){
		this.silenceListeners();
	}
	
	silenceListeners() {
		const eid = this.state.activeEvent ? this.state.activeEvent.eventID : null;
		if (!eid) { return; }
		if (this.basicInfoRef) db.ref(BASE_URL).child('basicInfo').off();
		if (this.defaultsRef) db.ref(BASE_URL).child('defaults').off();
		if (this.eventListRef) db.ref(BASE_URL).child('events').off();
		if (this.orderRef) db.ref(BASE_URL).child('orders').off();
		if (this.bookListRef) db.ref(BASE_URL).child('books').off();
		if (this.eventRef) db.ref(`${BASE_URL}/events/${eid}`).off(); 
		if (this.bookRef) db.ref(`${BASE_URL}/books/${eid}`).off(); 
		if (this.tempEventRef) db.ref(`${BASE_URL}/tmp_event`).off();
		if (this.addonCartRef) db.ref(`${BASE_URL}/addon_carts/${eid}`).off();
	} 
	
	silenceOldListeners(eid) {
		if (!eid) { return; }
		if (this.eventRef) { db.ref(`${BASE_URL}/events/${eid}`).off(); this.eventRef = undefined; } 
		if (this.bookRef) { db.ref(`${BASE_URL}/books/${eid}`).off(); this.bookRef = undefined; }
		if (this.addonCartRef) { db.ref(`${BASE_URL}/addon_carts/${eid}`).off(); this.addonCartRef = undefined; }
	}
	
	//=======================
	// new user: initial setup
	// auth, stripe, then database
	//=======================

	handleSignUp(e, p, f, l) {
		var context = this;
		var userTpl = User((new Date()).getTime());
		userTpl.basicInfo.email = e;
		userTpl.basicInfo.firstName = f;
		userTpl.basicInfo.lastName = l;
		userTpl.basicInfo.displayName = f;
		this.setAuthHandler(userTpl);
		admin.createUserWithEmailAndPassword(e, p).catch(function(error) {
			if (error) {
				context.setError(error.message, Priority.INVALID);
				return;
			}
		});
	}
	
	createNewUser(userTpl) {
		var context = this;
		ajax({
			method: 'POST',
			url: '/admin/customer',
			data: {
				email: userTpl.basicInfo.email,
				uid: userTpl.userID
			}
		}).done(function(resp) {
		}).fail(function(resp) {
			context.setError(resp, Priority.ERROR);
		}).always(function(resp) {
		});
	}
	
	handleLogout(completionHandler) {
		if (this.unlisten) { this.unlisten(); }
		admin.signOut().catch(function(error) {
			if (error) {
				window.setError(error.message, Priority.WARN);
			}
		}).then(function(){
			localStorage.setItem('mdgb-user-logged-in', 'false');
			completionHandler();
		});
	}
	
	//=======================
	// database api and listeners
	//=======================
	
	listenToBasicInfo(uid) {
		var context = this;
		const url = `${BASE_URL}/basicInfo`;
		if (this.basicInfoRef) { db.ref(url).off(); }
		this.basicInfoRef = db.ref(url).on('value', function(snapshot) {
			var data = snapshot.val();
			const nextState = rootReducer(context.state, act(BASIC_INFO_CHANGED, data));
			context.restate(nextState);
		});
	}
	
	listenToDefaults(uid) {
		var context = this;
		const url = `${BASE_URL}/defaults`;
		if (this.defaultsRef) { db.ref(url).off(); }
		this.defaultsRef = db.ref(url).on('value', function(snapshot) {
			var data = snapshot.val();
			const nextState = rootReducer(context.state, act(DEFAULTS_CHANGED, data));
			context.restate(nextState);
		});
	}
	
	listenToEvents(uid) {
		var context = this;
		const url = `${BASE_URL}/events`;
		if (this.eventListRef) { db.ref(url).off(); }
		this.eventListRef = db.ref(url).on('value', function(snapshot) {
			var data = snapshot.val();
			snapshot.forEach(function(childSnap) {
				var childKey = childSnap.key;
				if (childKey == context.state.activeID) {
					var childData = childSnap.val();
					const nextState = rootReducer(context.state, act(ACTIVE_EVENT_MODIFIED, childData));
					context.restate(nextState);
				}
			});
		});
	}
	
	refreshEvents(uid, eid) {
		var context = this;
		const url = `${BASE_URL}/events`;
		db.ref(url).once('value', function(snapshot) {
			var data = snapshot.val();
			snapshot.forEach(function(childSnap) {
				var childKey = childSnap.key;
				if (childKey == context.state.activeID) {
					var childData = childSnap.val();
					const nextState = rootReducer(context.state, act(REFRESH_EVENT_DATA, {list: childData, eid: eid}));
					context.restate(nextState);
				}
			});
		});
	}
	
	listenToActiveOnly(uid, eid) {
		if (this.eventRef) { db.ref(`${BASE_URL}/events/${eid}`).off(); }
		if (this.bookRef) { db.ref(`${BASE_URL}/books/${eid}`).off(); } 
		if (this.addonCartRef) { db.ref(`${BASE_URL}/addon_carts/${eid}`).off(); }
		var context = this;
		this.eventRef = db.ref(`${BASE_URL}/events/${eid}`).on('value', function(snapshot) {
			var data = JSON.parse(JSON.stringify(snapshot.val()));
			//const nextState = rootReducer(context.state, act(ACTIVE_EVENT_MODIFIED, data));
			context.restate(rootReducer(context.state, act(ACTIVE_EVENT_MODIFIED, data)));
		});
		this.bookRef = db.ref(`${BASE_URL}/books/${eid}`).on('value', function(snapshot) {
			var data = JSON.parse(JSON.stringify(snapshot.val()));
			//const nextState = rootReducer(context.state, act(ACTIVE_BOOK_MODIFIED, {book: data, eid: eid}));
			context.restate(rootReducer(context.state, act(ACTIVE_BOOK_MODIFIED, {book: data, eid: eid})));
		});
		this.addonCartRef = db.ref(`${BASE_URL}/addon_carts/${eid}`).on('value', function(snapshot) {
			var data = snapshot.val();
			const nextState = rootReducer(context.state, act(ADDON_CART_UPDATED, data));
			context.restate(nextState);
		});
	}
	
	listenToTmp(uid) {
		var context = this;
		const url = `${BASE_URL}/tmp_event`;
		if (this.tempEventRef) { db.ref(url).off(); }
		this.tempEventRef = db.ref(url).on('value', function(snapshot) {
			var data = snapshot.val();
			const nextState = rootReducer(context.state, act(TEMP_EVENT_MODIFIED, data));
			context.restate(nextState);
		});
	}
	
	listenToBooks(uid) {
		var context = this;
		const url = `${BASE_URL}/books`;
		if (this.bookListRef) { db.ref(url).off(); }
		this.bookListRef = db.ref(url).on('value', function(snapshot) {
			var data = snapshot.val();
			const nextState = rootReducer(context.state, act(ACTIVE_BOOK_MODIFIED, data));
			context.restate(nextState);
		});
	}
	
	fetchInitialUserData(uid) {
		var context = this;
		var newUser = false;
		db.ref(BASE_URL).once('value', function(snapshot) {
			var data = snapshot.val();
			if (!data.basicInfo) { 
				data.basicInfo = context.extractFromDeprecatedData(data); 
				context.writeToDatabase(act(UPDATE_OLD_ACCOUNT, data.basicInfo));
			}
			if (!data.tmp_event) {
				data.tmp_event = {};
			}
			if (context.detectIfNewUser(data.basicInfo.created)) { newUser = true; }
			const nextState = rootReducer(context.state, act(INITIAL_USER_DATA, { userID: uid, userData: data, isNew: newUser }));
			context.restate(nextState, true);
		}).catch(function(error) {
			context.setError(error.message, Priority.ERROR);
		});
	}
	
	extractFromDeprecatedData(userData) {
		if (userData.created && (new Date(userData.created)).getTime() < 1477000000000) {
			userData.created = Math.floor(newData.basicInfo.created * 1000)
		}
		return {
			firstName: userData.firstName,
			lastName: userData.lastName,
			created: userData.created ? userData.created : (new Date()).getTime(),
			customerID: userData.customerID ? userData.customerID : 'required',
			displayName: userData.displayName ? userData.displayName : userData.firstName,
			email: userData.email,
			phone: userData.phone ? userData.phone : '',
			photoURL: userData.photoURL ? userData.photoURL : '',
			newUser: true
		};
	}
	
	//=======================
	// server actions api
	//=======================

	serverAction(action) {
		switch(action.type) {
			case PHOTOSET_SUBMISSION:
				// send off to server
				
				break;
			default:
				break;
		}
	}
	
	postServerData(postData, endpoint) {
		ajax({
			url: `/admin/${endpoint}`,
			method: 'POST',
			data: postData
		}).fail(function(resp) {
			window.setError('There was an error submitting your photoset: ' + resp.message, Priority.ERROR);
		}).done(function(resp) {
			// display good stuff now
		}).always(function(resp) {
			console.log(resp);
		});
	}

	//=======================
	// write-to-storage api actions
	//=======================
	
	writeToStorage(action, data, isString, isTemp) {
		switch(action.type) {
			case COVER_IMAGE_ADDED:
				if (isString) {
					this.uploadAsString(data, action.payload.filetype, action.payload.toEvent, isTemp);
					return;
				}							
				this.uploadCoverImage(data, action.payload.filetype, action.payload.toEvent, isString, isTemp);
				break;
			default:
				break;
		}
	}
	
	//=======================
	// write-to-database api actions
	//=======================
	
	writeToDatabase(action, eid) {
		var context = this;
		switch(action.type) {
			case FIRST_CHANGED:
				db.ref(BASE_URL).child('basicInfo').child('firstName').set(action.payload);
				break;
			case LAST_CHANGED:
				db.ref(BASE_URL).child('basicInfo').child('lastName').set(action.payload);
				break;
			case PHONE_CHANGED:
				db.ref(BASE_URL).child('basicInfo').child('phone').set(action.payload);
				break;
			case DEFAULTS_CHANGED:
				db.ref(BASE_URL).child('defaults').update(action.payload);
				break;
			case SHIPPING_CHANGED: 
				db.ref(BASE_URL).child('defaults').child('shipping').update(action.payload);
				break;
			case SWITCH_DESIGN_TAB:
				db.ref(BASE_URL).child('web_activity').child('designTab').set(action.payload);
				break;
			case CREATE_INDEX_CHANGE: 
				db.ref(BASE_URL).child('web_activity').child('creationIndex').set(action.payload);
				break;
			case CART_INDEX_CHANGE:
				db.ref(BASE_URL).child('web_activity').child('cartIndex').set(action.payload);
				break;
			case CART_ACTION:
				db.ref(BASE_URL).child('web_activity').child('cartActions').push(action.payload);
				break;
			case COVER_TITLE_POSITIONED:
				db.ref(BASE_URL).child('books').child(eid).child('titlePosition').set(action.payload);
				break;
			case EVENT_NAME_EDITED:
				db.ref(BASE_URL).child('events').child(eid).child('eventName').set(action.payload);
				break;
			case EVENT_DATE_EDITED: 
				db.ref(BASE_URL).child('events').child(eid).child('eventDate').set(action.payload);
				break;
			case DATE_FORMAT_CHOSEN:
				db.ref(BASE_URL).child('events').child(eid).child('dateString').set(action.payload);
				break;
			case EVENT_PHOTO_CHANGED: 
				db.ref(BASE_URL).child('books').child(eid).child('urls').set(action.payload.urls);
				db.ref(BASE_URL).child('books').child(eid).child('leftOut').set(action.payload.leftOut);
				break;
			case SET_BOOK_URLS:
				db.ref(BASE_URL).child('books').child(eid).child('urls').set(action.payload);
				break;
			case COVER_TEXT_EDITED:
				const endpoint = action.payload.endpoint;
				const value = action.payload.value;
				db.ref(`${BASE_URL}/books/${eid}/coverText`).child(endpoint).set(value);
				break;
			case PHOTOSET_ADDITION:
				//this.uploadPhotosetAddition(action.payload.imageData, action.payload.mimeType);
				break;
			case COVER_IMAGE_ADDED:
				this.uploadCoverImage(action.payload.imageData, action.payload.mimeType);
				break;
			case COVER_IMAGE_UPLOADED:
				db.ref(`users/${context.state.userID}/events/${eid}/coverImage`).set(uploadRef.snapshot.downloadURL);
				db.ref(`users/${context.state.userID}/books/${eid}/coverImage`).set(uploadRef.snapshot.downloadURL);
				break;
			case SET_STOCK_COVER:
				db.ref(`users/${context.state.userID}/events/${eid}/coverImage`).set(action.payload);
				db.ref(`users/${context.state.userID}/books/${eid}/coverImage`).set(action.payload);
				break;
			case TEMP_TEXT_EDITED:
				const tmpEndpoint = action.payload.endpoint;
				const tmpVal = action.payload.value;
				db.ref(`${BASE_URL}/tmp_event/coverText`).child(tmpEndpoint).set(tmpVal);
				break;
			case SET_TEMP_COVER:
				db.ref(`users/${context.state.userID}/tmp_event/coverImage`).set(action.payload);
				break;
			case SET_TEMP_STOCK:
				db.ref(`users/${context.state.userID}/tmp_event/coverImage`).set(action.payload);
				break;
			case TEMP_NAME_EDITED:
				db.ref(`users/${context.state.userID}/tmp_event/eventName`).set(action.payload);
				break;
			case TEMP_DATE_EDITED:
				var d = action.payload;
				db.ref(`users/${context.state.userID}/tmp_event`).update({ eventDate: d, dateString: d });
				break;
			case TEMP_IMAGE_EDITED:
				db.ref(`users/${context.state.userID}/tmp_event/coverImage`).set(action.payload);
				break;
			case SET_COVER_COLORS: 
				db.ref(BASE_URL).child('books').child(eid).child('colors').set(action.payload);
			case USER_SIGNUP_EMAIL:
				db.ref(BASE_URL).set(action.payload).then(function() {
					context.createNewUser(action.payload);
					context.fetchInitialUserData(action.payload.userID);
				});
				break;
			case UPDATE_OLD_ACCOUNT:
				db.ref(BASE_URL).child('basicInfo').set(action.payload);
				db.ref(BASE_URL).child('web_activity').set(context.state.web_activity);
				db.ref(BASE_URL).child('app_activity').set(false);
				db.ref(BASE_URL).child('authorized_events').set(false);
				db.ref(BASE_URL).child('defaults').set(context.state.defaults);
				db.ref(BASE_URL).child('uid').set(context.state.userID);
				break;
			case ADDON_CART_EDIT:
				db.ref(BASE_URL).child('addon_carts').child(eid).set(action.payload);
				break;
			default:
				return;
		}
	}
	
	saveAddonToCart(item, delta) {
		//console.log('ADDON ITEM ----->', item, delta);
	}
	
	pingEventID(newEvent) {
		var context = this;
		ajax({
			url: '/admin/create',
			method: 'POST',
			data: {
				uid: context.state.userID
			}
		}).fail(function(err) {
			window.setError(err.message, Priority.ERROR);
		}).done(function(resp) {
			newEvent.eventID = resp.key;
			newEvent.code = resp.code;
			newEvent.created = resp.created;
			if (resp.coverImage) { newEvent.coverImage = resp.coverImage; }
			context.newEvent(newEvent);
		}).always(function(resp) {
			//console.log(resp);
		});
	}
	
	newEvent(newEvent) {
		var context = this;
		db.ref('users').child(context.state.userID).child('events').child(newEvent.eventID).set(
			newEvent
		).then(function() {
			db.ref('users').child(context.state.userID).child('books').child(newEvent.eventID).set({
				colors: { text: '#ffffff', bg: 'transparent' },
				coverText: newEvent.coverText,
				titlePosition: 'middle',
				eventID: newEvent.eventID,
				coverImage: newEvent.coverImage
			}).then(function() {
				const eid = newEvent.eventID;
				window.location.assign(`/cart?e=${eid}`);
			});
		});
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~
	// TODO: consolidate this upload mess
	//~~~~~~~~~~~~~~~~~~~~~~~
		
	uploadPhotosetAddition(data, mime, nameWithExt, eid, isString, isTemp) {
		var context = this;
		const md = {
			contentType: mime	
		};
		const uid = this.state.userID;
		if (isString) {
			this.uploadAdditionAsString(data, mime, nameWithExt, eid);
			return;
		}
		const uploadPath = isTemp ? `console/${uid}/tmp/${nameWithExt}` : `console/${uid}/${eid}/${nameWithExt}`;
		var ref = storage.ref().child('console').child(uid).child(eid).child(nameWithExt);
		ref.put(data, md).then(function(snapshot) {
			var dlURL = snapshot.downloadURL;
			context.storeAdditionLink(dlURL, eid);
		}).catch(function(error) {
			if (error) {
				//console.log(error);
				window.setError('There was an error uploading your image. Please try again.', Priority.INVALID);	
			}
		});
	}
	
	uploadAdditionAsString(data, mime, nameWithExt, eid, isTemp) {
		var context = this;
		const md = {
			contentType: mime	
		};
		const uid = this.state.userID;
		var uploadData = data.substring(data.indexOf('base64,') + 7, data.length);
		var ref = storage.ref().child('console').child(uid).child(eid).child(nameWithExt);
		ref.putString(uploadData, 'base64').then(function(snapshot) {
			var dlURL = snapshot.downloadURL;
			ref.updateMetadata(md);
			context.storeAdditionLink(dlURL, eid);
		}).catch(function(error) {
			if (error) {
				//console.log(error);
				window.setError('There was an error uploading your image. Please try again.', Priority.INVALID);	
			}
		});	
	}
	
	storeAdditionLink(url, eid) {
		const uid = this.state.userID;
		db.ref('users').child(uid).child('events').child(eid).child('additions').push(url);
	}
	
	storeCoverLink(url, eid) {
		const uid = this.state.userID;
		db.ref('events').child(uid).child(eid).child('coverImage').set(url);
		db.ref('users').child(uid).child('events').child(eid).child('coverImage').set(url);
		db.ref('users').child(uid).child('books').child(eid).child('coverImage').set(url);
	}
	
	storeTempCoverLink(url) {
		const uid = this.state.userID;
		db.ref('users').child(uid).child('tmp_event').child('coverImage').set(url);
	}
	
	uploadCoverImage(data, mime, eid, isString, isTemp) {
		const uid = this.state.userID;
		var context = this;
		const md = {
			contentType: mime	
		};
		var nameWithExt = 'coverImage.' + mime.split('/')[1];
		const uploadPath = isTemp ? `console/${uid}/tmp/${nameWithExt}` : `console/${uid}/${eid}/${nameWithExt}`;
		if (isString) {
			this.uploadAsString(data, mime, eid, isTemp);
			return;
		}
		var ref = storage.ref().child(uploadPath);
		ref.put(data, md).then(function(snapshot) {
			var dlURL = snapshot.downloadURL;
			if (!isTemp) { context.storeCoverLink(dlURL, eid); }
			else if (isTemp) { context.storeTempCoverLink(dlURL); }
		}).catch(function(error) {
			if (error) {
				console.log(error);
				window.setError('There was an error uploading your image. Please try again.', Priority.INVALID);	
			}
		});
	}
	
	uploadAsString(data, mime, eid, isTemp) {
		var context = this;
		const md = {
			contentType: mime	
		};
		var nameWithExt = 'coverImage.' + mime.split('/')[1];
		const uid = this.state.userID;
		var uploadData = data.substring(data.indexOf('base64,') + 7, data.length);
		const uploadPath = isTemp ? `console/${uid}/tmp/${nameWithExt}` : `console/${uid}/${eid}/${nameWithExt}`;
		var ref = storage.ref().child(uploadPath);
		ref.putString(uploadData, 'base64').then(function(snapshot) {
			var dlURL = snapshot.downloadURL;
			ref.updateMetadata(md);
			if (!isTemp) { context.storeCoverLink(dlURL, eid); console.log('not temp'); }
			else if (isTemp) { context.storeTempCoverLink(dlURL); }
		}).catch(function(error) {
			if (error) {
				console.log(error);
				window.setError('There was an error uploading your image. Please try again.', Priority.INVALID);	
			}
		});	
	}

	
	//=======================
	// user auth api
	//=======================
	
	setAuthHandler(userTpl) {
		var context = this;
		if (this.authHandle) { this.authHandle(); this.authHandle = undefined; }
		this.authHandle = admin.onAuthStateChanged(function(user) {
			if (user) {
				if (user.isAnonymous) {
					//handleAnonUser(user);
					return;
				}
				localStorage.setItem('mdgb-user-logged-in', 'true');
				//window.checkUserStatus();
				BASE_URL = `users/${user.uid}`;
				if (userTpl) {
					userTpl.userID = user.uid;
					context.writeToDatabase(act(USER_SIGNUP_EMAIL, userTpl));
				}
				else {
					context.fetchInitialUserData(user.uid);	
				}
				history.replace('/create', {tabIdx: 0});			
			}
			else {
				localStorage.setItem('mdgb-user-logged-in', 'false');
			}
		});
	}
	
	sendReset(e) {
		admin.sendPasswordResetEmail(e).then(function() {
		}, function(error) {
			if (error) {
				window.setError(error.message, Priority.INVALID);
			}
		});
	}
	
	detectIfNewUser(created) {
		const d = new Date();
		var diff = (d - (new Date(created))) / 1000;
		if (diff < 259200) { return true; }	
	}

	//=======================
	// child callbacks
	//=======================
	
	handleLogin(e, p) {
		var context = this;
		admin.signInWithEmailAndPassword(e, p).catch(function(error) {
			if (error) {
				context.setError('Please double check your credentials and try again.', Priority.INVALID);
				//context.handleMenuClick(5);
			}
		});
	}
	
	pushMenuHistory(tabIdx) {
		let routes = ['create', 'design', 'tips', 'account', 'support', 'login'];
		history.push(`/${routes[tabIdx]}`, {tabIdx: tabIdx});
	}
	
	handleMenuClick(tabIdx, shouldClose) {
		if (tabIdx === (this.state.menuOptions.length - 1)) { this.handleLogout(function() {window.location.reload();}); }
		var menuAction = act(TOGGLE_MENU, {tabIdx: tabIdx, shouldClose: shouldClose});
		const nextState = rootReducer(this.state, menuAction);
		this.restate(nextState);
	}
	
	chooseArea(areaID) {
		this.writeToDatabase(act(COVER_TITLE_POSITIONED, areaID), this.state.activeEvent.eventID);
	}
	
	handleDesignTab(value) {
		this.writeToDatabase(act(SWITCH_DESIGN_TAB, value), this.state.activeEvent.eventID);
	}
	
	handleCreateStep(value) {
		this.writeToDatabase(act(CREATE_INDEX_CHANGE, value), this.state.activeEvent.eventID);
	}
	
	handleFirst(first) {
		this.writeToDatabase(act(FIRST_CHANGED, first));
	}
	
	handleLast(last) {
		this.writeToDatabase(act(LAST_CHANGED, last));
	}
	
	handlePhone(phone) {
		this.writeToDatabase(act(PHONE_CHANGED, phone));
	}
	
	handleDateChange(date) {
		this.writeToDatabase(act(EVENT_DATE_EDITED, date), this.state.activeEvent.eventID);
	}
	
	handleName(val) {
		this.writeToDatabase(act(EVENT_NAME_EDITED, val), this.state.activeEvent.eventID);
	}
	
	handleDateFormatSelect(date) {
		this.writeToDatabase(act(DATE_FORMAT_CHOSEN, date), this.state.activeEvent.eventID);
	}
	
	showUserEvents(evt) {
		console.log(evt.target);
	}
	
	handleShippingChange(shippingInfo) {
		const nextState = rootReducer(this.state, act(SHIPPING_CHANGED, 'account:shipping'));
		this.restate(nextState);
		this.writeToDatabase(act(SHIPPING_CHANGED, shippingInfo), false);
	}
	
	handlePhotosetAdd(data, type) {
		this.uploadPhotosetAddition(data, type);
	}
	
	startSubmission() {
		const uid = this.state.userID;
		const eid = this.state.activeEvent.eventID;
		const email = this.state.basicInfo.email;
		const data = { uid: uid, eid: eid, email: email };
		this.postServerData(data, 'photosubmit');
	}

	toggleSelect(leaveOut, photoID) {
		var leftOutList = this.state.activeBook.leftOut;
		var urlsList = this.state.activeBook.urls;
		if (leaveOut) {
			leftOutList.push(photoID);
			const idx = urlsList.indexOf(this.state.activeEvent.urls[photoID]);
			urlsList.splice(idx, 1);
		}
		else {
			urlsList.push(this.state.activeEvent.urls[photoID]);
			const idx = leftOutList.indexOf(photoID);
			leftOutList.splice(idx, 1);
		}
		this.writeToDatabase(act(EVENT_PHOTO_CHANGED, {leftOut: leftOutList, urls: urlsList}), this.state.activeEvent.eventID);
	}
	
	setBookPhotoLinks() {
		const urlList = this.state.activeEvent.urls;
		var urlArray = [];
		for (var urlKey in urlList) {
			urlArray.push(urlList[urlKey]);
		}
		this.writeToDatabase(act(SET_BOOK_URLS, urlArray), this.state.activeEvent.eventID);
	}
	
	toggleMenu(evt) {
		const nextState = rootReducer(this.state, act(TOGGLE_MENU, {tabIdx: -1, shouldClose: false}));
		this.restate(nextState);
	}
	
	selectActiveEvent(eventKey) {
		this.silenceOldListeners(this.state.activeEvent.eventID);
		const nextState = rootReducer(this.state, act(CHOOSE_ACTIVE_EVENT, this.state.events[eventKey]));
		this.restate(nextState, false, true);
	}
	
	handleTitles(endpoint, value) {
		this.writeToDatabase(act(COVER_TEXT_EDITED, {endpoint: endpoint, value: value}), this.state.activeEvent.eventID);
	}
	
	handleCoverColors(textColor, bgColor) {
		this.writeToDatabase(act(SET_COVER_COLORS, { text: textColor, bg: bgColor }), this.state.activeEvent.eventID);
	}
	
	handleStockChange(src) {
		this.writeToDatabase(act(SET_STOCK_COVER, src), this.state.activeEvent.eventID);
	}
	
	handleTempName(val) {
		this.writeToDatabase(act(TEMP_NAME_EDITED, val));
	}
	
	handleTempDate(date) {
		var d = date.toLocaleDateString();
		this.writeToDatabase(act(TEMP_DATE_EDITED, d), false);
	}
	
	handleTempTitles(endpoint, value) {
		this.writeToDatabase(act(TEMP_TEXT_EDITED, { endpoint: endpoint, value: value }));
	}
	
	handleTempStockChange(src) {
		this.writeToDatabase(act(SET_TEMP_STOCK, src));
	}
	
	handleTempImageUpload(url) {
		console.log('tmp upload img');
	}
	
	handleSelectArea(areaID) {
		this.writeToDatabase(act(COVER_TITLE_POSITIONED, areaID), this.state.activeEvent.eventID);
	}
	
	handleAddon(sku, delta) {
		var newCart = [];
		var cartString = JSON.parse(JSON.stringify(this.state.addonCart));
		if (cartString[sku]) {
			if (cartString[sku].quantity + delta > 0) {
				cartString[sku].quantity += delta;
			}
			else { delete cartString[sku]; }
		}
		else if (delta > 0) {
			cartString[sku] = { sku: sku, quantity: delta, price: catalog[sku].price };
		}
		for (var s in cartString) {
			newCart.push({ sku: s, quantity: cartString[s].quantity, price: catalog[s].price });
		}
		this.writeToDatabase(act(ADDON_CART_EDIT, newCart), this.state.activeEvent.eventID);
	}
	
	startAddonOrder(numPages) {
		var context = this;
		ajax({
			url: '/cart/edit',
			method: 'GET',
			data: {
				uid: context.state.userID,
				oid: context.state.activeEvent.orderID
			}
		}).fail(function(resp) {
			
		}).done(function(resp) {
			
		}).always(function(resp) {
			
		});
	}
	
	//=======================
	// global error handling
	//=======================
	
	setError(message, priority) {
		console.warn(message);
		var context = this;
		const nextState = rootReducer(this.state, act(DISPLAY_USER_ALERT, {message: message, priority: priority}));
		this.restate(nextState);
		setTimeout(function() {
			context.hideError();
		}, 7000);
	}
	
	hideError() {
		const nextState = rootReducer(this.state, act(HIDE_USER_ALERT, null));
		this.restate(nextState);
	}
		
	//=======================
	// app component renders
	//=======================
	
	renderMenu() {
		return (
			<div className="console-left">
				<div className="side-panel">
					<EventMarquee 
						err={this.setError}
						activeEvent={this.state.activeEvent} 
						showUserEvents={this.showUserEvents} 
						toggleMenu={this.toggleMenu}
						events={this.state.events}
						selectActiveEvent={this.selectActiveEvent} 
						activeTabIdx={this.state.activeTabIdx} />
					<Menu 
						err={this.setError}
						handleClick={this.pushMenuHistory}
						menuOpen={this.state.menuOpen} 
						menuOptions={this.state.menuOptions} 
						activeTabIdx={this.state.activeTabIdx} />
				</div>
			</div>
		);
	}
	
	renderAuth() {
		return (
			<AuthWrapper 
				err={this.setError} 
				handleLogin={this.handleLogin} 
				handleSignUp={this.handleSignUp} 
				handleLogout={this.handleLogout} 
				forgotPassword={this.forgotPassword} 
				admin={admin} />
		);
	}
		
	renderConsoleRight() {
		return <MainBody 
					err={this.setError}
					userID={this.state.userID}
					saving={this.state.saving}
					activeTabIdx={this.state.activeTabIdx} 
					activeEvent={this.state.activeEvent} 
					activeBook={this.state.activeBook} 
					tempEvent={this.state.tempEvent}
					chooseArea = {this.chooseArea} 
					selectedArea={this.state.activeBook.titlePosition} 
					handleCreateStep={this.handleCreateStep}
					handleTabChange={this.handleDesignTab} 
					handleDateChange={this.handleDateChange} 
					basicInfo={this.state.basicInfo} 
					defaults={this.state.defaults} 
					handleDateFormatSelect={this.handleDateFormatSelect}
					handleName={this.handleName} 
					orders={this.state.orders}
					handleFirst={this.handleFirst}
					handleLast={this.handleLast} 
					handlePhone={this.handlePhone}
					handleShippingChange={this.handleShippingChange}
					handleTitles={this.handleTitles} 
					handleCoverColors={this.handleCoverColors} 
					writeToStorage={this.writeToStorage}
					pingEventID={this.pingEventID} 
					handleBookURLs={this.handleBookURLs} 
					toggleSelect={this.toggleSelect} 
					writeToDatabase={this.writeToDatabase} 
					startAddonOrder={this.startAddonOrder} 
					saveAddonToCart={this.saveAddonToCart}
					handleSelectArea={this.handleSelectArea} 
					viewOrderDetails={this.viewOrderDetails} 
					startSubmission={this.startSubmission} 
					setBookPhotoLinks={this.setBookPhotoLinks} 
					handleTempName = {this.handleTempName}
					handleTempDate = {this.handleTempDate}
					handleTempTitles = {this.handleTempTitles}
					handleTempStockChange = {this.handleTempStockChange}
					handleTempStockChange = {this.handleTempStockChange}
					handleTempImageUpload = {this.handleTempImageUpload} 
					handleAddon={this.handleAddon} />
	}
	
	//=======================
	// base render
	//=======================
	
	render(){
		let user = admin.currentUser;
		var menuComponent = user ? this.renderMenu() : null;
		//var welcomeComponent = 	this.state.newUser && this.state.activeTabIdx == 3 ? <WelcomeUser err={this.setError} /> : null;
		var mainComponent = user ? this.renderConsoleRight(this.state.newUser) : this.renderAuth();
		return (
			<div id="app" className="app">
				{menuComponent}
				<div className="console-right full-view">
					{mainComponent}
				</div>
				<UserAlert error={this.state.error} />
			</div>
		)
	}
	
};

export default App;