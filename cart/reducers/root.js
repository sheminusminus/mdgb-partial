import {
	GOT_ACTIVE_ID, GOT_PREVIEW_URL, ORDER_ID_ADDED,
	ADD_ITEM, REMOVE_ITEM, ADD_PROMO, RECEIVED_TOKEN,
	CREATED_ORDER, CREATED_SOURCE, REVIEW_AMOUNT, SUBMIT_PAYMENT,
	INITIAL_CART_DATA, CART_CHANGED, CHARGE_RESPONSE, START_CHECKOUT, 
	ADVANCE_CHECKOUT, REVERSE_CHECKOUT, DISPLAY_USER_ALERT
} from '../action-types';

import initialDataReducer from './initial-data';

var products = require('../../json/skus.json')[window.ENV];

var cartItems = function(state, payload) {
	var newState = (JSON.parse(JSON.stringify(state)));
	if (!payload || !payload.filter(containsBook)) { 
		newState.cart = [{quantity: 1, sku: products.book, price: 175}]; 
		newState.hasBase = true;
	}
	else { 
		newState.cart = payload;
		if (newState.cart.filter(containsBook)) { newState.hasBase = true; }
	}
	return newState;
}

function containsBook(item) {
	return item.sku == products['book'];
}

var promoCode = function(state, payload) {
	var newState = (JSON.parse(JSON.stringify(state)));
	if (payload) {
		newState.couponID = payload.code;
		newState.discount = payload.value;	
		newState.message = payload.message;
	}
	else {
		newState.couponID = '';
		newState.discount = 0;
		newState.message = '';
	}
	return newState;
}

var errorMessageReducer = function(state, action) {
	state.error = action.payload;
	return state;
}

var newCustomerOrder = function(state, payload) {
	var newState = (JSON.parse(JSON.stringify(state)));
	newState.orderID = payload.orderID;
	newState.message = payload.message;
	newState.amount = payload.amount;
	newState.stage = 2;
	newState.stepTitle = 'Payment Info';
	return newState;
}

var newSourceToken = function(state, payload) {
	var newState = (JSON.parse(JSON.stringify(state)));
	newState.token.clientIP = payload.client_ip;
	newState.token.id = payload.id;
	newState.token.used = payload.used;
	return newState;
}

var newSourceCard = function(state, payload) {
	var newState = (JSON.parse(JSON.stringify(state)));
	newState.sourceID = payload.sourceID;
	newState.last4 = payload.last4;
	newState.stage = 3;
	newState.stepTitle = 'Review Order';
	return newState;
}

var orderPayment = function(state, payload) {
	var newState = (JSON.parse(JSON.stringify(state)));
	newState.stage = 4;
	newState.stepTitle = payload.status == 'paid' ? 'Thank you!' : payload.status;
	return newState;
}

var updateActive = function(state, action) {
	switch(action.type) {
		case GOT_ACTIVE_ID:
			var newState = (JSON.parse(JSON.stringify(state)));
			newState.activeID = action.payload.eid;
			newState.previewURL = action.payload.book.coverImage;
			return newState;
		default:
			return state;
	}
}

var setFromOrderID = function(state, payload) {
	var newState = (JSON.parse(JSON.stringify(state)));
	var add = payload.shipping.address;
	newState.line1 = add.line1;
	newState.line2 = add.line2;
	newState.city = add.city;
	newState.postal = add.postal_code;
	newState.email = payload.email;
	newState.amount = payload.amount;
	var cartItems = [];
	for (var item in payload.items) {
		if (item.type != 'sku') { 
			
		}
		cartItems.push({quantity: item.quantity, sku: item.parent, price: item.amount});
	}
	newState.cart = cartItems;
	newState.orderID = payload.id;
	return newState;
}

var returnTitle = function(payload) {
	switch(payload) {
		case 0:
			return '';
		case 1:
			return 'Shipping Info';
		case 2: 
			return 'Payment Method';
		case 3: 
			return 'Review Selections';
		default:
			return 'Hooray!';
	}
}

var rootReducer = function(state = 0, action) {
	console.log(state);
	console.log('ACTION: ' + action.type + '\nPAYLOAD: ', action.payload);
	var pay = action.payload;
	switch(action.type) {
		case INITIAL_CART_DATA:
			var newState = initialDataReducer(state, action.payload);
			return newState;
		case CART_CHANGED:
			return cartItems(state, action.payload);
		case ADD_PROMO:
			return promoCode(state, action.payload);
		case START_CHECKOUT:
			var newState = (JSON.parse(JSON.stringify(state)));
			newState.stage = action.payload;
			return newState;
		case ADVANCE_CHECKOUT:
			var newState = (JSON.parse(JSON.stringify(state)));
			newState.stage = action.payload;
			newState.stageTitle = returnTitle(action.payload);
			return newState;
		case REVERSE_CHECKOUT:
			var newState = (JSON.parse(JSON.stringify(state)));
			newState.stage = action.payload;
			newState.stageTitle = returnTitle(action.payload);
			return newState;
		case ORDER_ID_ADDED:
			var newState = (JSON.parse(JSON.stringify(state)));
			newState.orderID = action.payload;
			return newState;
		case CREATED_ORDER:
			return newCustomerOrder(state, action.payload);
		case RECEIVED_TOKEN:
			return newSourceToken(state, action.payload);
		case CREATED_SOURCE:
			return newSourceCard(state, action.payload);
		case CHARGE_RESPONSE:
			return orderPayment(state, action.payload);
		case GOT_ACTIVE_ID: 
			return updateActive(state, action);
		case GOT_PREVIEW_URL:
			return updateActive(state, action);
		case DISPLAY_USER_ALERT:
			return errorMessageReducer(state, action);
		default:
			return state;
	}
}

export default rootReducer;