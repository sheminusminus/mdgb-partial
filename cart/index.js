import React from 'react';
import firebase from 'firebase';
import { ajax } from 'jquery';
import NavigationArrowDownward from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import NavigationArrowForward from 'material-ui/svg-icons/navigation/arrow-forward';
import NavigationArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper';
import NavigationCheck from 'material-ui/svg-icons/navigation/check';
import FontIcon from 'material-ui/FontIcon';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import createHistory from 'history/createBrowserHistory';

const history = createHistory({
	basename: '/cart'
});

var db, storage, admin, BASE_URL, EID;

// action types and creator
import act from './create-actions';
import {
	GOT_ACTIVE_ID, GOT_PREVIEW_URL, ORDER_ID_ADDED,
	ADD_ITEM, REMOVE_ITEM, CLEAR_CART, ADD_PROMO, RECEIVED_TOKEN,
	CREATED_ORDER, CREATED_SOURCE, REVIEW_AMOUNT, SUBMIT_PAYMENT,
	INITIAL_CART_DATA, CART_CHANGED, PROMO_CHANGED, START_CHECKOUT,
	ADVANCE_CHECKOUT, REVERSE_CHECKOUT, CHANGE_ITEM,
	DISPLAY_USER_ALERT
} from './action-types';

// reducer
import rootReducer from './reducers/root';

// enums
import { Priority } from './constants';

const ENV = window.ENV;
const catalog = require('../json/catalog.json')[ENV];
const skus = require('../json/skus.json')[ENV];

// components
import Checkout from './Checkout';
import CartItem from './CartItem';
import CartInfoItem from './CartInfoItem';
import PromoEntry from './PromoEntry';
import Catalog from './Catalog';
import ShippingForm from './ShippingForm';
import PaymentForm from './PaymentForm';
import ReviewForm from './ReviewForm';
import ConfirmOrder from './ConfirmOrder';
import CoverThumbnail from './CoverThumbnail';
import AddonOption from './AddonOption';
import ShippingOption from './ShippingOption';

class Cart extends React.Component{
	
	constructor(props) {
		super(props);
		this.state = {
			sectionClasses: 'step-loaded',
			first: '',
			last: '',
			line1: '',
			line2: '',
			city: '',
			postal: '',
			state: '',
			phone: '',
			email: '',
			card: '',
			last4: '',
			cvc: '',
			month: '',
			year: '',
			zip: '',
			amount: 0,
			stage: 0,
			orderID: '',
			sourceID: '',
			customerID: '',
			discount: 0,
			couponID: '',
			uid: '',
			cart: [],
			message: '',
			hasBase: false,
			promoInput: '',
			activeID: '',
			eventName: '',
			previewURL: '',
			error: {},
			stageTitle: 'Shipping Information',
			dialogOpen: false,
			token: {
				id: '',
				used: false,
				clientIP: ''
			}
		};
		
		// firebase managers
		db = firebase.database();
		admin = firebase.auth();
		
		// error display
		window.setError = this.setError.bind(this);
		
		// callbacks
		this.openPromoDialog = this.openPromoDialog.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.add = this.add.bind(this);
		this.remove = this.remove.bind(this);
		this.handlePromo = this.handlePromo.bind(this);
		this.applyPromo = this.applyPromo.bind(this);
		this.advanceCheckout = this.advanceCheckout.bind(this);
		this.reverseCheckout = this.reverseCheckout.bind(this);
		this.makeOrder = this.makeOrder.bind(this);
		this.makeSource = this.makeSource.bind(this);
		this.set = this.set.bind(this);
		this.countItem = this.countItem.bind(this);
		this.backToDash = this.backToDash.bind(this);
		this.first = this.first.bind(this);
		this.last = this.last.bind(this);
		this.line1 = this.line1.bind(this);
		this.line2 = this.line2.bind(this);
		this.city = this.city.bind(this);
		this.shipState = this.shipState.bind(this);
		this.postal = this.postal.bind(this);
		this.email = this.email.bind(this);
		this.shipPhone = this.shipPhone.bind(this);
		this.card = this.card.bind(this);
		this.cvc = this.cvc.bind(this);
		this.month = this.month.bind(this);
		this.year = this.year.bind(this);
		this.zip = this.zip.bind(this);
		this.onToken = this.onToken.bind(this);
	}
	
	componentDidMount() {
		this.setAuthListener();
		this.unlisten = history.listen(function(location, action) {
			context.handleMenuClick(location.state.tabIdx, true);	
        });
	}
	
	restate(nextState, firstSet) {
		// replace state with new state object
		this.setState(
			nextState,
			function() {
				//console.info('---> new state:', this.state);
				if (!firstSet) { return; }
				this.listenToCartData(nextState.activeID);		
			}		
		);
	}
	
	setAuthListener() {
		var context = this;
		var eid = '';
		if (this.authHandle) { this.authHandle(); this.authHandle = undefined; }
		this.authHandle = admin.onAuthStateChanged(function(user) {
			if (user) {
				BASE_URL = `users/${user.uid}`;
				const query = window.location.search.split('?')[1];
				if (query.split('=')[0] === 'e') { eid = query.split('=')[1]; }
				localStorage.setItem('mdgb-user-logged-in', 'true');
				context.fetchUserData(user.uid, eid);
			}
			else {
				localStorage.setItem('mdgb-user-logged-in', 'false');
			}
		});
	}
	
	userSignOut() {
		admin.signOut().then(function() {
			localStorage.setItem('mdgb-user-logged-in', 'false');
		}, function(err) {
			//console.log(error);
			window.setError({ message: err.message, priority: Priority.WARN });
		});
	}
	
	fetchUserData(uid, eid) {
		var context = this;
		db.ref(BASE_URL).once('value', function(snapshot) {
			var data = snapshot.val();
			var oid = '';
			if (data.events && data.events[eid] && data.events[eid].orderID) {
				oid = data.events[eid].orderID
			}
			const nextState = rootReducer(context.state, act(INITIAL_CART_DATA, { userID: uid, userData: data, eid: eid, oid: oid }));
			context.restate(nextState, true);
		}).catch(function(error) {
			//context.setError(error);
			window.setError({ message: err.message, priority: Priority.ERROR });
		});
		db.ref(BASE_URL).child('tmp_event').set(false);
	}
	
	clearCartItems() {
		db.ref(`${BASE_URL}/cart`).set(false);
	}

	listenToCartData(activeID) {
		var context = this;
		if (this.cartRef) { db.ref(`${BASE_URL}/cart`).off('value'); this.cartRef = undefined; }
		this.cartRef = db.ref(`${BASE_URL}/cart`).on('value', function(snapshot) {
			var data = snapshot.val();
			const nextState = rootReducer(context.state, act(CART_CHANGED, data));
			context.restate(nextState);
		});
		if (this.orderLinkRef) { 
			db.ref(`${BASE_URL}/events/${activeID}/orderID`).off('value'); 
			this.orderLinkRef = undefined; 
		}
		this.orderLinkRef = db.ref(`${BASE_URL}/events/${activeID}/orderID`).on('value', function(snapshot) {
			var data = snapshot.val();
			if (context.state.orderID == '' && data && data.length) {
				// TODO: MAKE THIS ACTION:
				const nextState = rootReducer(context.state, act(ORDER_ID_ADDED, data));
				context.restate(nextState);
				if (nextState.orderID && nextState.orderID != context.state.orderID) {
					this.askStripeForEventCartItems(nextState.orderID);
				}
			}
		});
		if (this.promoRef) { db.ref(BASE_URL).child('cart_promo').off('value'); this.promoRef = undefined; }
		this.promoRef = db.ref(BASE_URL).child('cart_promo').on('value', function(snapshot) {
			var data = snapshot.val();
			const nextState = rootReducer(context.state, act(ADD_PROMO, data));
			context.restate(nextState);
		});
	}
	
	countBooks() {
		var skus = this.props.skuList;
		var count = 0;
		for (var i = 0; i < this.state.cart.length; i++) {
			if (this.state.cart[i].sku === skus.book) {
				count++;
			}
			else if (this.state.cart[i].sku === skus.hard || this.state.cart[i].sku === skus.soft) {
				count += parseInt(this.state.cart[i].quantity);
			}	

		}
		return count;
	}
	
	renderCartItems() {
		var items = [];
		if (!this.state.cart.length) {
			items.push(<p key={'k-emptyCart'} className="empty-cart">Nothing here yet! Add the Base Package below to get started.</p>);
			return items;
		}
		for (var i = 0; i < this.state.cart.length; i++) {
			var sku = this.state.cart[i].sku;
			var k = 'cartItem-' + i;
			var itemQ = parseInt(this.state.cart[i].quantity);
			var item$ = parseInt(this.state.cart[i].price);
			var isBook = (sku == this.props.skuList.book);
			var lineItem$ = itemQ * item$;
			if (sku === this.props.skuList.hPhoto) {
				lineItem$ *= this.countBooks();
			}
			items.push(<CartItem key={k} quantity={itemQ} price={lineItem$} title={catalog[sku].title} description={catalog[sku].description} sku={sku} add={this.add} remove={this.remove} isBook={isBook} />);
		}
		return items;
	}
	
	renderShippingLine() {
		//var items = [];
		//var cartItems = this.state.cart;
		//var skus = Object.keys(cartItems);
		if (!this.state.cart.length) {
			return null;
		}
		//items.push(<CartInfoItem key={'shipLine'} title={'Shipping (Free to U.S. Contiguous 48)'} price={0} />);
		return (<CartInfoItem key={'shipLine'} title={'Shipping (Free to U.S. Contiguous 48)'} price={'$' + 0} />);
	}
	
	renderPromoLine() {
		if (this.state.discount > 0) {
			return (<CartInfoItem key={'promoLine'} title={this.state.message} price={'-$' + this.state.discount} />);
		}
		return null;
	}
	
	renderTotalLine() {
		var items = [];
		var total = 0;
		if (!this.state.cart.length) {
			return null;
		}
		for (var i = 0; i < this.state.cart.length; i++) {
			var sku = this.state.cart[i].sku;
			var itemQ = parseInt(this.state.cart[i].quantity);
			var lineItem$ = itemQ * parseInt(this.state.cart[i].price);
			if (sku === this.props.skuList.hPhoto) {
				lineItem$ *= this.countBooks();
			}
			total += lineItem$;
		}
		total -= parseInt(this.state.discount);
		items.push(<CartInfoItem key={'totalLine'} title={'Total'} price={'$' + total} />);
		return items;
	}
	
	renderPromoEntry() {
		return (<PromoEntry key={'promoInput'} promoInput={this.state.promoInput} handlePromo={this.handlePromo} applyPromo={this.applyPromo} />);
	}
	
	renderCheckout() {
		if (!this.state.cart.length) {
			return null;
		}
		return (<Checkout 
					key={'checkout'} 
					cart={this.state.cart} 
					uid={this.state.uid} 
					email={this.state.email} 
					confirm={this.state.confirm} 
					couponID={this.state.couponID} 
					customerID={this.state.customerID} 
					stage={this.state.stage}
					onShip={this.makeOrder}
					onPay={this.makeSource}
					onReview={this.requestCharge}
					onStripe={this.orderResponse} />
		);
	}
	
	add(sku) {
		var added = false;
		var cart = this.state.cart;
		if (!this.state.hasBase) {
			cart = [{sku: this.props.skuList.book, price: 175, quantity: 1}];
			this.writeToDatabase(act(ADD_ITEM, cart));
		}
		if (sku === this.props.skuList.book) {
			return;
		}
		else {
			for (var i = 0; i < cart.length; i++) {
				if (cart[i].sku == sku) {
					cart[i].quantity += 1;
					added = true;
				}
			}
			if (!added) { 
				cart.push({
					sku: sku,
					quantity: 1,
					price: catalog[sku].price
				});
			}
		}
		//console.log(ADD_ITEM, cart);
		this.writeToDatabase(act(ADD_ITEM, cart));	
	}
	
	handlePromo(evt) {
		this.setState({
			promoInput: evt.target.value.toUpperCase()
		});
	}
	
	applyPromo() {
		var context = this;
		ajax({
			method: 'POST',
			url: '/cart/promo',
			data: {
				promo: context.state.promoInput,
				uid: context.state.uid
			}
		}).fail(function(resp) {
			window.setError({ message: err.message, priority: Priority.ERROR });
		}).done(function(resp) {
			// SET COUPON ID AND DISCOUNT STATE VALUES HERE
			
		}).always(function(resp) {
			//console.log(resp);
			context.handleClose();
		});
	}
	
	remove(sku) {
		var cart = this.state.cart ? this.state.cart : [];
		if (sku == this.props.skuList.book) {
			return;
		}
		for (var i = 0; i < cart.length; i++) {
			if (cart[i].sku == sku) {
				if (cart[i].quantity <= 1) {
					cart.splice(i, 1);
					continue;
				}
				cart[i].quantity -= 1;
			}
		}
		this.writeToDatabase(act(REMOVE_ITEM, cart));
	}
	
	set(sku, value) {
		if (value < 0) {
			value = 0;
		}
		var cart = this.state.cart;
		if (sku == this.props.skuList.book) {
			return;
		}
		for (var i = 0; i < cart.length; i++) {
			if (cart[i].sku == sku) {
				cart[i].quantity = value;
			}
		}
		this.writeToDatabase(act(CHANGE_ITEM, cart));
	}
	
	writeToDatabase(action) {
		const CART_URL = `users/${this.state.uid}/cart`;
		switch(action.type) {
			case ADD_ITEM:
				db.ref(CART_URL).set(action.payload);
				break;
			case REMOVE_ITEM:
				db.ref(CART_URL).set(action.payload);
				break;
			case CLEAR_CART:
				db.ref(CART_URL).set(false);
				db.ref(BASE_URL).child('cart_promo').set(false);
				break;
			case ADD_PROMO:	
				db.ref(`${BASE_URL}/cart_promo`).set(action.payload);
				break;
			case CHANGE_ITEM:
				db.ref(CART_URL).set(action.payload);
				break;
			default:
				return;	
		}
	}
	
	formatShipping() {
		var c = this.state;
		return {
			name: c.first + ' ' + c.last,
			address: {
				line1: c.line1,
				line2: c.line2,
				city: c.city,
				country: 'US',
				postal_code: c.postal
			},
			phone: c.phone ? c.phone : ''
		};
	}
	
	first(evt) {
		this.setState({
			first: evt.target.value
		});
	}
	
	last(evt) {
		this.setState({
			last: evt.target.value
		});
	}
	
	line1(evt) {
		this.setState({
			line1: evt.target.value
		});
	}
	
	line2(evt) {
		this.setState({
			line2: evt.target.value
		});
	}
	
	city(evt) {
		this.setState({
			city: evt.target.value
		});
	}
	
	shipState(evt) {
		this.setState({
			state: evt.target.value
		});
	}
	
	postal(evt) {
		this.setState({
			postal: evt.target.value
		});
	}
	
	shipPhone(evt) {
		this.setState({
			phone: evt.target.value
		});
	}
	
	email(evt) {
		this.setState({
			email: evt.target.value
		});
	}
	
	card(val) {
		this.setState({
			card: val
		});
	}
	
	last4(val) {
		this.setState({
			last4: val
		});
	}
	
	cvc(val) {
		this.setState({
			cvc: val
		});		
	}
	
	month(val) {
		this.setState({
			month: val
		});
	}
	
	year(val) {
		this.setState({
			year: val
		});
	}
	
	zip(val) {
		this.setState({
			zip: val
		});
	}
		
	advanceCheckout(evt) {
		evt.preventDefault();
		//console.log(this.state.stage);
		var nextStage = this.state.stage;
		var s = this.state;
		switch(this.state.stage) {
			case 0:
				nextStage = 1;
				break;
			case 1:
				if (!s.first || !s.last || !s.line1 || !s.city || !s.state || !s.postal || !s.phone || !s.email) {
					alert('Please fill out all the required fields.');
				}
				else if (s.first == '' || s.last == ''  || s.line1 == ''  || s.city == ''  || s.state == ''  || s.postal == ''  || s.phone == ''  || s.email == '' ) {
					alert('Please fill out all the required fields.');
				}
				else {
					this.makeOrder();
					nextStage = 2;	
				}
				break;
			case 2:
				if (!s.card || !s.month || !s.year || !s.cvc || !s.zip) {
					alert('All billing fields are required.');
				}
				else if (s.card == '' || s.month == '' || s.year == '' || s.cvc == '' || s.zip == '') {
					alert('All billing fields are required.');
				}
				else {
					this.makeSource();
					nextStage = 3;	
				}
				break;
			case 3:
				this.payOrder();
				nextStage = 4;
				break;
			case 4:
				nextStage = 5;
				break;
			default: 
				break;
		}
		const nextState = rootReducer(this.state, act(ADVANCE_CHECKOUT, nextStage));
		this.restate(nextState);
	}
	
	reverseCheckout(evt) {
		if (this.state.stage == 0 || this.state.stage == 5) {
			window.location.assign('/admin');
		}
		const nextState = rootReducer(this.state, act(REVERSE_CHECKOUT, this.state.stage - 1));
		this.restate(nextState);
	}
	
	makeOrder() {
		var context = this;
		var s = this.state;

		var orderShipping = this.formatShipping();
		var orderData =  {
			items: s.cart,
			shipping: orderShipping,
			orderEmail: s.email,
			couponID: s.couponID,
			eventID: s.activeID,
			uid: s.uid
		};
		if (s.customerID) { orderData.customer = s.customerID }
		//console.log(this.state.cart, this.state.couponID, orderShipping, this.state.email, this.state.customerID);
		ajax({
			method: 'POST',
			url: '/cart/orders/new',
			data: orderData
		}).fail(function(err){
			window.setError({ message: err.message, priority: Priority.ERROR });
		}).done(function(resp) {
			const nextState = rootReducer(context.state, act(CREATED_ORDER, {
				orderID: resp.orderID,
				amount: parseInt(resp.cost) / 100,
				message: resp.message
			}));
			context.restate(nextState);
		}).always(function(resp) {
			//console.log(resp);
		})	
	}
	
	makeSource() {
		var context = this;
		var form = {
		  	number: this.state.card,
		  	exp_month: this.state.month,
		  	exp_year: this.state.year,
		  	cvc: this.state.cvc,
		  	address_zip: this.state.zip
	  	};
	  	window.Stripe.card.createToken(form, this.onToken);
	}
	
	onToken(status, response) {
		//console.log(status, response);
		const nextState = rootReducer(this.state, act(RECEIVED_TOKEN, {id: response.id, client_ip: response.client_ip, used: response.used }));
		this.restate(nextState);
		this.createCustomerSource(response.id);
	}
	
	createCustomerSource(src) {
		var context = this;
		ajax({
			method: 'POST',
			url: '/cart/source',
			data: {
				sourceID: src,
				customerID: context.state.customerID
			}
		}).fail(function(err){
			window.setError({ message: err.message, priority: Priority.ERROR });
		}).done(function(resp) {
			var cardID = resp.id;
			var last4 = resp.last4;
			const nextState = rootReducer(context.state, act(CREATED_SOURCE, {sourceID: cardID, last4: last4}));
			context.restate(nextState);
		}).always(function(resp) {
			//console.log(resp);
		})
	}
	
	calcPhotos() {
		var photos = 192;
		for (var i = 0; i < this.state.cart.length; i++) {
			if (this.state.cart[i].sku == skus['hPhoto']) {
				photos += (parseInt(this.state.cart[i].quantity) * 32);
			}
		}
		return photos;
	}
	
	payOrder() {
		var context = this;
		const photos = this.calcPhotos();
		const desc = `Guestbook with ${photos} event photos`;
		ajax({
			method: 'POST',
			url: '/cart/charge',
			data: {
				summary: desc,
				orderID: context.state.orderID,
				customer: context.state.customerID,
				uid: context.state.uid,
				eid: context.state.activeID
			}
		}).fail(function(err){
			window.setError({ message: err.message, priority: Priority.ERROR });
		}).done(function(resp) {
			//console.log('success');
		}).always(function(resp) {
			//console.log(resp);
		});
	}

	
	returnCartSection(photosetQ, hardQ, softQ, shipRate) {
		const liStyles = {margin: '0 0 0 0', padding: '0 0 0 0', height: '50px', justifyContent: 'center'};
		const iconStyles = {color: 'rgb(255,255,255)', backgroundColor: 'rgb(255,255,255)', fill: 'rgb(255,255,255)'};
		const eventName = this.state.eventName != '' ? this.state.eventName : 'Base Package';
		return (
			<div className="cart-app-body">
				<div className="cart-header">
					<CoverThumbnail source={this.state.previewURL} />
					<div className="base-desc">
						<h5 className="cart-title">{eventName}</h5>
						<h4 className="cart-title">$139</h4>
						<ul className="base-package-checklist">
							<li className="cart-list-item">
								<NavigationCheck className="cart-nav-check" color="rgb(255,255,255)"/>
								<span className="checklist-item">192 Photos</span>
							</li>
							<li className="cart-list-item">
								<NavigationCheck className="cart-nav-check" color="rgb(255,255,255)"/>
								<span className="checklist-item">1 Hardcover</span>
							</li>
							<li className="cart-list-item">
								<NavigationCheck className="cart-nav-check" color="rgb(255,255,255)"/>														<span className="checklist-item">App Access</span>
							</li>
						</ul>
					</div>
				</div>
				<div className="options-wrapper">
					<AddonOption quantity={photosetQ} add={this.add} remove={this.remove} set={this.set} item={catalog[this.props.skuList.hPhoto]} />
					<AddonOption quantity={hardQ} add={this.add} remove={this.remove} set={this.set} item={catalog[this.props.skuList.hard]} />
					<AddonOption quantity={softQ} add={this.add} remove={this.remove} set={this.set} item={catalog[this.props.skuList.soft]} />
					<ShippingOption item={shipRate} />
				</div>
			</div>
		);
	}
	
	getShoppingSections(stage) {
		const photosetQ = this.countItem(skus['hPhoto']);
		const hardQ = this.countItem(skus['hard']);
		const softQ = this.countItem(skus['soft']);
		const shipRate = this.shippingRate();
		const subtotal = this.getSubtotal(photosetQ, hardQ, softQ, shipRate.price);
		var cartView = this.returnCartSection(photosetQ, hardQ, softQ, shipRate);
		const actions = this.getActionButtons(this.state.stage, subtotal);
		var container = {
		};
		if (window.innerWidth < 768) { container.flexDirection = 'column'; }
		switch(stage) {
			case 0:
				return (
					<div style={container} className="cart-flex-controller">
						<div className="cart-area small-12 medium-12 large-12">
							{cartView}
							{actions}
						</div>
					</div>
				);
			case 1:
				return (
					<div style={container} className="cart-flex-controller">
						<div className="form-area small-12 medium-12 large-12">
							<h6>Step {this.state.stage + 1} of 5: {this.state.stageTitle}</h6>
							<ShippingForm submit={this.makeOrder} first={this.first} last={this.last} line1={this.line1} line2={this.line2} cit={this.city} shipState={this.shipState} postal={this.postal} f={this.state.first} l={this.state.last} l1={this.state.line1} l2={this.state.line2} c={this.state.city} s={this.state.state} p={this.state.postal} phone={this.state.phone} e={this.state.email} shipPhone={this.shipPhone} email={this.email} city={this.city} />
						</div>
						{actions}
					</div>
				);
			case 2:
				return (
					<div style={container} className="cart-flex-controller">
						<div className="form-area small-12 medium-12 large-12">
							<h6>Step {this.state.stage + 1} of 5: {this.state.stageTitle}</h6>
							<PaymentForm submit={this.makeSource} card={this.card} month={this.month} year={this.year} zip={this.zip} cvc={this.cvc} c={this.state.card} mm={this.state.month} yy={this.state.year} z={this.state.zip} cv={this.state.cvc} />
						</div>
						{actions}
					</div>
				)
			case 3:
				return (
					<div style={container} className="cart-flex-controller">
						<div className="form-area small-12 medium-12 large-12">
							<h6>Step {this.state.stage + 1} of 5: {this.state.stageTitle}</h6>
							<ReviewForm amount={this.state.amount} last4={this.state.last4} f={this.state.first} l={this.state.last} l1={this.state.line1} l2={this.state.line2} c={this.state.city} s={this.state.state} p={this.state.postal} promo={this.state.couponID} discount={this.state.discount} last4={this.state.last4} />	
						</div>
						{actions}
					</div>
				);
			case 4:
				return (
					<div style={container} className="cart-flex-controller">
						<div className="form-area small-12 medium-12 large-12">
							<h6>Step {this.state.stage + 1} of 5: {this.state.stageTitle}</h6>
							<ConfirmOrder orderID={this.state.orderID} email={this.state.email} />
						</div>
						{actions}
					</div>
				)
			default:
				return null;
		}
	}
	
	nextButtonText(stage) {
		switch(stage) {
			case 0:
				return "Ready to Checkout";
			case 5:
				return "Complete";
			default:
				return "Next";	
		}
	}
	
	countItem(itemSKU) {
		if (!this.state.cart) { return 0; }
		for (var i = 0; i < this.state.cart.length; i++) {
			if (this.state.cart[i].sku === itemSKU) {
				return this.state.cart[i].quantity;
			}
		}
		return 0;
	}
	
	shippingRate() {
		if (!this.state.cart) { return 0; }
		for (var i = 0; i < this.state.cart.length; i++) {
			if (this.state.cart[i].type === 'shipping') {
				return {
					price: this.state.cart[i].amount / 100,
					title: this.state.cart[i].description
				};
			}
		}
		return {
			price: 10,
			title: 'Flat-rate shipping'
		};
	}
	
	getSubtotal(pq, hq, sq, ship) {
		var sub = 139;
		sub += (hq * 47);
		sub += (sq * 22);
		sub += (((1 + hq + sq) * 5) * pq);
		sub += ship;
		sub -= this.state.discount;
		return sub;
	}
	
	backToDash() {
		window.location.assign('/admin');
	}
	
	openPromoDialog(evt) {
		evt.preventDefault();
		this.setState({dialogOpen: true});
	}
	
	getActionButtons(stage, subtotal) {
		if (stage < 1) {
			return (
				<div className="cart-actions">
					<h1>${subtotal}</h1>
					<FloatingActionButton className="checkout-btn button next" onClick={this.advanceCheckout} id="advance" backgroundColor="#2ea3f2">
				      <NavigationArrowForward />
				    </FloatingActionButton>
				</div>
			);
		}
		else if (stage < 4) {
			return (
				<div className="cart-actions">
					<FloatingActionButton className="checkout-btn button set back" onClick={this.reverseCheckout} id="reverse" backgroundColor="#81c7f7">
				      <NavigationArrowBack />
				    </FloatingActionButton>
				    <FloatingActionButton className="checkout-btn button set next" onClick={this.advanceCheckout} id="advance" backgroundColor="#2ea3f2">
				      <NavigationArrowForward />
				    </FloatingActionButton>
				</div>
			);
		}
		else {
			return (
				<div className="cart-actions">
					<button className="checkout-btn button set dash" onClick={this.backToDash} id="advance">{"Awesome. Let's go!"}</button>
				</div>
			);
		}
	}
	
	callStripe(action) {
		
	}
	
	askStripeForEventCartItems(orderID) {
		// TODO: this
		var context = this;
		console.info('calling stripe for order items');
		ajax({
			url: '/cart/get',
			method: 'POST',
			data: {
				orderID: orderID,
				uid: context.state.uid
			}
		})fail(function(err){
			window.setError({ message: err.message, priority: Priority.INVALID });
		}).done(function(resp) {
			
		}).always(function(resp) {
			//console.log(resp);
		});
	}
	
	handleClose(evt) {
		if (evt) { evt.preventDefault(); }
		this.setState({
			dialogOpen: false
		});
	}
	
	//=======================
	// global error handling
	//=======================
	
	setError(message, priority) {
		var context = this;
		const nextState = rootReducer(this.state, act(DISPLAY_USER_ALERT, {message: message, priority: priority}));
		this.restate(nextState);
		setTimeout(function() {
			context.hideError();
		}, 7000);
	}
	
	hideError() {
		const nextState = rootReducer(this.state, act(HIDE_USER_ALERT, null));
		this.restate(nextState);
	}
	
	renderPromoRelatedUI() {
		const actions = [
	      <FlatButton
	        label="Cancel"
	        primary={false}
	        keyboardFocused={false}
	        onTouchTap={this.handleClose}
	      />,
	      <FlatButton
	        label="Apply"
	        primary={true}
	        keyboardFocused={true}
	        onTouchTap={this.applyPromo}
	      />
	    ];
		var buttonBg = this.state.couponID ? '#F9B390' : '#CBD6E1';
		var buttonText = this.state.couponID ? this.state.couponID : 'Use Promo';	    
		return (
			<div>
				<Dialog
				  title="Enter Promo Code"
				  actions={actions}
				  modal={false}
				  open={this.state.dialogOpen}
				  onRequestClose={this.handleClose}>
				  <TextField hintText="Code" value={this.state.promoInput} onChange={this.handlePromo} />
				</Dialog>
				<FlatButton label={buttonText} onClick={this.openPromoDialog} style={{position: 'absolute', bottom: '30px', left: '20px', backgroundColor: buttonBg}} labelStyle={{color: 'rgba(0,0,0,0.6)'}}>
				</FlatButton>
			</div>
		);
	}
	
	render() {
		const section = this.getShoppingSections(this.state.stage);
		var container = {
			display: 'flex',
			flexDirection: 'row'
		};
		const promoLabel = this.state.couponID != '' ? this.state.couponID : 'Use Promo';
		const promoUI = this.state.stage > 0 ? null : this.renderPromoRelatedUI();
		if (window.innerWidth < 768) { container.flexDirection = 'column'; }
		return (
			<div className="cart-body small-12 medium-8 large-6" style={{margin: '15px auto', padding: '0px'}}>
				<MuiThemeProvider>
					<Paper zDepth={3} rounded={false} style={{paddingBottom: '10px', position: 'relative'}}>
						{section}
						{promoUI}
					</Paper>
				</MuiThemeProvider>
			</div>
		);
	}
}

export default Cart;