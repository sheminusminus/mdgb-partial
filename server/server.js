
//===============
// TODO: VENDOR INVITE CODES
//===============

'use strict';

//===============
// airbrake
// (prod only)
//===============

/*
try { var airbrakecfg = require('../etc/airbrakeconfig'); } catch(e) { }
if (airbrakecfg && (airbrakecfg.enabled || process.env['AIRBRAKE'])) {
	var airbrake = require('airbrake').createClient(airbrakecfg.id, airbrakecfg.key);
	airbrake.handleExceptions();
	airbrake.notify(new Error('Server started'), function (err, url) {
		if (err) throw err;
		console.log('Airbrake enabled: ' + url);
	});
}
*/


var getArg = function(i, default_value) {
    if (process.argv.length > i+2) {
        return process.argv[i+2];
    } else {
        return default_value;
    }
}

const path = require('path');
const fs = require('fs');
const https = require('https');
const image_downloader = require('image-downloader');
const request = require('request');
const zipFolder = require('zip-folder');
const PORT = parseInt(getArg(0, '3000'));
const express = require('express');
const bodyParser = require('body-parser');
const serveStatic = require('serve-static');
const rewrite = require('express-urlrewrite');

//===============
// routers
//===============

const app = express();
const router = express.Router();
const cart = express.Router();
const updates = express.Router();

app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', serveStatic('dist', {'extensions': ['html']}), function(req, res, next) {
    next();
});

// all routes for admin pass through router
app.use('/admin', router);
// all routes for cart pass through cart
app.use('/cart', cart);
// all routes for stripe webhooks pass through orders
app.use('/updates', updates);

//===============
// stripe
//===============

var stripeconfig = require('../etc/stripeconfig');
var stripe = require("stripe")(stripeconfig.test);
//var stripe = require("stripe")(stripeconfig.live);

//===============
// skus
//===============

const testItems = require('../etc/skusTest');
const prodItems = require('../etc/skusLive');
const catalog = require('../etc/catalog');
app.set('products', catalog.test);
//app.set('products', testItems.products);

var multer = require('multer');
var upload = multer();

//===============
// jwt
//===============
var jwt = require('jsonwebtoken');
var jwtconfig = require('../etc/jwtconfig');
app.set('jwtvar', jwtconfig.secret);

//===============
// moment
//===============
var moment = require('moment');

//===============
// mailgun
//===============
var mgconfig = require('../etc/mailgunconfig');
app.set('mgvar', mgconfig.apikey);
var Mailgun = require('mailgun').Mailgun;
var mg = new Mailgun(app.get('mgvar'));
var api_key = app.get('mgvar');

//===============
// dropbox
//===============
var Dropbox = require('dropbox');
const dbconfig = require('../etc/dbconfig');
const db_token = dbconfig.token;
var dbx = new Dropbox({accessToken: db_token});

//===============
// firebase
//===============
var admin = require("firebase-admin");

var serviceAccount = './etc/mdgb-efd99ab84f26.json';

var fire = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://egbdev-38305.firebaseio.com/",
    databaseAuthVariableOverride: {
        uid: "secretsquirrel"
    }
});

const db = admin.database();
const auth = admin.auth();

//===============
// misc endpoints
//===============

app.use('/terms', serveStatic('dist/assets/userterms.pdf', {'extensions': ['pdf']}));

//===============
// orders (stripe) endpoints
//===============

updates.post('/status', function(req, res, next) {
	const type = req.body.type;
	const uid = req.body.data.object.metadata.uid ? req.body.data.object.metadata.uid : '';
	var updates = req.body.data.object;
	console.log('----->status:\n', updates, '\n----->user:\n', uid);
	res.sendStatus(200);
});

updates.post('/paid', function(req, res, next) {
	const type = req.body.type;
	const uid = req.body.data.object.metadata.uid ? req.body.data.object.metadata.uid : '';
	const eid = req.body.data.object.metadata.event_id ? req.body.data.object.metadata.event_id : '';
	const addon = req.body.data.object.metadata.addon != undefined ? req.body.data.object.metadata.addon : false;
	console.log('----->addon:\n', addon, '\n----->user:\n', uid);
	res.sendStatus(200);
});

updates.post('/addon', function(req, res, next) {
	var isAddon = req.body.data.object.metadata.addon != undefined ? req.body.data.object.metadata.addon : false;
	var order = req.body.data.object;
	var shipID = req.body.data.object.selected_shipping_method;
	if (!isAddon) {
		res.sendStatus(200);
		return;	
	}
	console.log(order.id);	
	order.shipping_methods.forEach(function(method) {
		if (method.amount == 0) {
			shipID = method.id;	
		}
	})
	stripe.orders.update(order.id, {selected_shipping_method: shipID}, function(err, updated) {
		if (err) { console.log(err); return; }
		if (updated) {
			console.log(updated);
		}
	})
});

//===============
// cart endpoints
//===============

cart.use(serveStatic('dist/cart.html', {'extensions': ['html']}), function(req, res, next) {
	next();
});

cart.use('/assets/stock_thumbs/*', serveStatic('dist/assets/stock_thumbs', {
	'extensions': ['png', 'jpg', 'jpeg', 'gif']
}));

cart.use('/edit', serveStatic('dist/cart.html', {'extensions': ['html']}), function(req,res,next) {
	const uid = req.body.uid;
	const oid = req.body.oid;
	const pgs = 3;
	stripe.orders.retrieve(oid, function(err, order) {
		if (err) { console.log(err); res.send(err); }
		else if (order) {
			const originalAdd = parseInt(order.metadata.photosets_added) * 32;
			const eid = order.metadata.event_id;	
			db.ref(`users/${uid}/events/${eid}`).once('value', function(snapshot) {
				var photoData = snapshot.val().urls;
				var listLength = Object.keys(photoData).length;
				const magicNumber = listLength - originalAdd;
				db.ref(`users/${uid}/cart`).set({
					quantity: magicNumber,
					price: 5,
					sku: 'A'
				});
			});
		}
	})
});

cart.post('/charge', function(req, res) {
	var uid = req.body.uid;
	var custID = req.body.customer;
    var summary = req.body.summary;
    var eid = req.body.eid;
    stripe.orders.pay(req.body.orderID, {
	    customer: custID
        //source: req.body.source
    }, function(err, order) {
        if (err) { res.send(err); }
        if (order && order.status == 'paid') {
	        var totalHardcovers = calcHardcovers(order.items);
	        var totalSoftcovers = calcSoftcovers(order.items);
	        var totalPhotos = calcPhotos(order.items, (totalHardcovers + totalSoftcovers + 1));
	        order['photos'] = totalPhotos;
            db.ref('users').child(uid).child('orders').child(order.id).set(order);
            db.ref('users').child(uid).child('cart').set(false);
            db.ref('users').child(uid).child('cart_promo').set(false);
            db.ref('users').child(uid).child('events').child(eid).update({
	            photos: totalPhotos,
	            hardcovers: totalHardcovers,
	            softcovers: totalSoftcovers
            });
            var eventCode = customString(9);
            summary += ', ' + totalPhotos + ' Photos';
            //createUserEvent(uid, eventCode, summary, order.created, order.id, totalPhotos);
            var data = {
                order: order,
                eventCode: eventCode
            }
            res.send(data);
        }
    });
});

cart.use('/promo', function(req, res, next) {
	var promo = req.body.promo.toLowerCase();
	var uid = req.body.uid;
	console.log(req.body);
	stripe.coupons.retrieve(promo, function(err, coupon) {
		if (err) { 
			console.log(err); 
			sendPromoResponse(res, '', 0, 0, ''); 
			return; 
		}
		else if (!coupon.valid) {
			sendPromoResponse(res, '', 0, 0, 'Invalid promo code.');
			return;
		}
		else {
			sendPromoResponse(res, coupon.id, (coupon.amount_off / 100), coupon.metadata.message);
			savePromoCodeToFirebase(uid, coupon.id, (coupon.amount_off / 100), coupon.metadata.message);	
		}
	});
});

cart.post('/get', function(req, res) {
	const uid = req.body.uid;
	const oid = req.body.orderID;
	console.log(uid, oid);
	stripe.orders.retrieve(oid, function(err, order) {
		if (err) { console.log(err); res.send({ message: err }); }
		else if (order) {
			console.log(order);
			const items = order.items;
			var cart = [];
			for (var i = 0; i < items.length; i++) {
				if (items[i].type === 'sku') {
					cart.push({
						sku: items[i].parent,
						quantity: parseInt(items[i].quantity),
						price: parseInt(items[i].amount) / parseInt(items[i].quantity)
					});
				}
			}
			if (cart.length > 0) { db.ref(`users/${uid}/cart`).set(cart); }
			else { db.ref(`users/${uid}/cart`).set(false); }
			res.send({ message: order });
		}
	});
});

cart.post('/source', function(req, res) {
	const cust = req.body.customerID;
	const source = req.body.sourceID;
	stripe.customers.createSource(cust, {source: source}, function(err, card) {
		if (err) {
			console.log(err);
			res.send({message: err});
		}
		else if (card) {
			console.log(card);
			res.send({id: card.id, last4: card.last4});
		}
	});
})

cart.post('/orders/new', function(req, res) {
	var reqItems = req.body.items;
	var shipInfo = req.body.shipping;
	var orderEmail = req.body.orderEmail;
	var forEventID = req.body.eventID;
	var customerID = req.body.customerID ? req.body.customerID : '';
	var uid = req.body.uid;
	var xBooks = 1;
    var items = [];
    if (!reqItems || reqItems == undefined || reqItems == '' || reqItems === {}) {
        res.send({cost: 0});
        return;
    }
    var promoCode = req.body.couponID ? req.body.couponID.toUpperCase() : '';
    var skus = Object.keys(reqItems);
    var photosetSKU = '';
    stripe.skus.list({}, function (err, response) {
	    for (var i = 0; i < reqItems.length; i++) {
		    var sku = reqItems[i]['sku'];
	        if (sku == 'promo') { continue; }
	        var q = parseInt(reqItems[i]['quantity']);
	        if (q < 1 || q == undefined) { continue; }
		    var j = 0;
		    for (; j < response.data.length; j++) {
			    var item = response.data[j];
			    if (item.attributes['books']) {
					photosetSKU = item.id;
				}
				if (sku === item.id) {						
					items.push({
						type: 'sku',
						parent: sku,
						quantity: q
					});
					if (response.data[j].attributes['cover']) {
						xBooks += q;
						console.log(xBooks);
					}
					break;
				}
				
		    }
		    if (j === response.data.length) { continue; }
		}
		var orderData = {
			currency: 'usd',
	        coupon: promoCode,
	        items: items,
	        shipping: shipInfo,
	        email: orderEmail,
	        customer: customerID,
	        metadata: {
		        event_id: forEventID,
		        uid: uid,
		        addon: false
	        }
		};
		for (var i = 0; i < items.length; i++) {
			if (items[i].parent === photosetSKU) {
				orderData.metadata['photosets_added'] = items[i].quantity;
				orderData.metadata['book_multiplier'] = (xBooks - 1);
				console.log(orderData.metadata);
				items[i].quantity *= orderData.metadata.book_multiplier;
			}
		}
		orderData.items = items;
		console.log(orderData);
		if (orderData.coupon == '') { delete orderData.coupon; }
		if (orderData.customer == '') { delete orderData.customer; }
		stripe.orders.create(
			orderData, 
			function(err, order) {
		        if (err) {
			        console.log(err);
			        sendOrderResponse(res, 'An error has occurred on charge.', 0, orderEmail, [], shipInfo, '', err, {uid: uid, eid: forEventID});
		        }
		        else if (order) {
			        sendOrderResponse(res, order.id, order.amount, order.email, order.items, order.shipping, order.shipping_method, 'Order created!', {uid: uid, eid: forEventID}); 
			    }
	    });
	});
});

cart.post('/orders/addon', function(req, res, next) {
	var reqItems = req.body.items;
	var shipInfo = req.body.shipping;
	var orderEmail = req.body.orderEmail;
	var forEventID = req.body.eventID;
	var customerID = req.body.customerID ? req.body.customerID : '';
	var originalOrder = req.body.originalOrder ? req.body.originalOrder : '';
	var uid = req.body.uid;
	var xBooks = 1;
    var items = [];
    if (!reqItems || reqItems == undefined || reqItems == '' || reqItems === {}) {
        res.send({cost: 0});
        return;
    }
    var promoCode = req.body.couponID ? req.body.couponID.toUpperCase() : '';
    var skus = Object.keys(reqItems);
    var photosetSKU = '';
    stripe.skus.list({}, function (err, response) {
	    for (var i = 0; i < reqItems.length; i++) {
		    var sku = reqItems[i]['sku'];
	        if (sku == 'promo') { continue; }
	        var q = parseInt(reqItems[i]['quantity']);
	        if (q < 1 || q == undefined) { continue; }
		    var j = 0;
		    for (; j < response.data.length; j++) {
			    var item = response.data[j];
			    if (item.attributes['books']) {
					photosetSKU = item.id;
				}
				if (sku === item.id) {						
					items.push({
						type: 'sku',
						parent: sku,
						quantity: q
					});
					if (response.data[j].attributes['cover']) {
						xBooks += q;
						console.log(xBooks);
					}
					break;
				}
				
		    }
		    if (j === response.data.length) { continue; }
		}
		var orderData = {
			currency: 'usd',
	        coupon: promoCode,
	        items: items,
	        shipping: shipInfo,
	        email: orderEmail,
	        customer: customerID,
	        metadata: {
		        event_id: forEventID,
		        uid: uid,
		        order_id: originalOrder,
		        addon: true
	        }
		};
		for (var i = 0; i < items.length; i++) {
			if (items[i].parent === photosetSKU) {
				orderData.metadata['photosets_added'] = items[i].quantity;
				orderData.metadata['book_multiplier'] = (xBooks - 1);
				console.log(orderData.metadata);
				items[i].quantity *= orderData.metadata.book_multiplier;
			}
		}
		orderData.items = items;
		console.log(orderData);
		if (orderData.coupon == '') { delete orderData.coupon; }
		if (orderData.customer == '') { delete orderData.customer; }
		stripe.orders.create(
			orderData, 
			function(err, order) {
		        if (err) {
			        console.log(err);
			        sendOrderResponse(res, 'An error has occurred on charge.', 0, orderEmail, [], shipInfo, '', err, {uid: uid, eid: forEventID});
		        }
		        else if (order) {
			        var shipID = order.selected_shipping_method;
			        console.log(order.shipping_methods);
			        order.shipping_methods.forEach(function(method) {
				       if (method.amount == 0) {
					       shipID = method.id;
				       } 
			        });
			        stripe.orders.update(order.id, { selected_shipping_method: shipID }, function(err, updated) {
				       sendOrderResponse(res, updated.id, updated.amount, updated.email, updated.items, updated.shipping, updated.shipping_method, 'Order created!', {uid: updated.metadata.uid, eid: updated.metadata.event_id});  
			        });
			    }
	    });
	});
});

cart.use('/orders/addon/charge', function(req,res) {
/*
	const uid = req.body.uid;
	const oid = req.body.oid;
	const pgs = 3;
	stripe.orders.retrieve(oid, function(err, order) {
		if (err) { console.log(err); res.send(err); }
		else if (order) {
			const originalAdd = parseInt(order.metadata.photosets_added) * 32;
			const eid = order.metadata.event_id;	
			db.ref(`users/${uid}/events/${eid}`).once('value', function(snapshot) {
				var photoData = snapshot.val().urls;
				var listLength = Object.keys(photoData).length;
				const magicNumber = listLength - originalAdd;
				db.ref(`users/${uid}/cart`).set({
					quantity: magicNumber,
					price: 5,
					sku: 'A'
				});
			});
		}
	})
*/
});

cart.use('/*', serveStatic('dist/cart.html', {'extensions': ['html']}));

//===============
// admin endpoints
//===============

router.use(serveStatic('dist', {'extensions': ['html']}), function(req, res, next) {
    next();
});

router.use('/assets/stock_thumbs/*', serveStatic('dist/assets/stock_thumbs', {
	'extensions': ['png', 'jpg', 'jpeg', 'gif']
}));

router.post('/customer', function(req, res) {
	var uid = req.body.uid;
	var email = req.body.email;
	stripe.customers.create({
        description: 'MDGB user for email ' + email,
        email: email
    }, function(err, customer) {
        if (err) { console.log(err); return; }
        if (customer) {
			db.ref('users').child(uid).child('basicInfo').child('customerID').set(customer.id);
			res.send({status: 'saved cid'});
        }
    });
});

router.post('/create', function(req, res) {
	var uid = req.body.uid;
	var eventKey = db.ref().child(`users/${uid}/events`).push().key;
	var eventCode = customString(9);
	var eventCreated = (new Date()).getTime();
	var sendData = {key: eventKey, code: eventCode, created: eventCreated};
	db.ref().child(`users/${uid}`).once('value').then(function(snapshot){
		const userData = snapshot.val();
		if (userData.tmp_cover){ sendData.coverImage = userData.tmp_cover; }
		console.log(sendData);
		res.send(sendData);
	}).catch(function(err) {
		console.log(err);
		res.send(err);
	});
})

router.post('/partial', function(req, res) {
	const uid = req.body.uid;
	const eventKey = db.ref(`users/${uid}/events`).push().key;
	res.send({key: eventKey});
})

router.post('/photosubmit', function(req, res) {
	var finishedTech = false;
	var finishedMGMT = false;
	const uid = req.body.uid;
	const userEmail = req.body.email;
	const eid = req.body.eid;
	const tech = 'technical@mydigitalguestbook.com';
	const mgmt = 'emkolar@gmail.com';
	const d = new Date();
	const internalSubject = 'GUESTBOOK PHOTOSET SUBMITTED';
	const userSubject = `Alright! We've received your MyDigitalGuestbook photoset.`;
	const bookbot = 'bookbot@mydigitalguestbook.com';
	var internalMsg = `USER INFO\n\nUser ID: ${uid} \n\nEmail: ${userEmail} \n\nEVENT INFO\n\nEvent ID: ${eid} \n\nSubmitted on: ${d.toString()} \n\nEvent .zips on Dropbox (allow 10+ minutes): https://www.dropbox.com/sh/zi2ig8phrdieygi/AADxo6NgWIju0Wb9ADoLueWJa?dl=0 \n\n`;
	const userMsg = `We'll be printing and shipping it ASAP. Check your inbox again soon for a tracking link!`;
	db.ref('users').child(uid).once('value').then(function(snapshot) {
		const urlObject = snapshot.val()['books'][eid]['urls'];
		const urlKeys = Object.keys(urlObject);
		const coverPhoto = snapshot.val()['books'][eid]['coverImage'];
		var urls = [];
		for (var i = 0; i < urlKeys.length; i++) { urls.push(urlObject[urlKeys[i]]); }
		mkEventDir(uid, eid, urls, coverPhoto);
	
		for (var i = 0; i < urls.length; i++) {
			
			internalMsg += urls[i] + '\n';
		}
		mg.sendText(bookbot, tech, internalSubject, internalMsg, '', {}, function(err) {
			finishedTech = true;
			if (err) {
				console.log('Tech email error: ' + err);
			}
			if (finishedTech && finishedMGMT) {
				db.ref('users').child(uid).child('events').child(eid).child('finalized').set(true);
			}
		});
		mg.sendText(bookbot, mgmt, internalSubject, internalMsg, '', {}, function(err) {
			finishedMGMT = true;	
			if (err) {
				console.log('MGMT email error: ' + err);
			}
			if (finishedTech && finishedMGMT) {
				db.ref('users').child(uid).child('events').child(eid).child('finalized').set(true);
			}
		});
		mg.sendText(bookbot, userEmail, userSubject, userMsg, '', {}, function(err) {
			if (err) {
				console.log('User email error: ' + err);
			}
			else {
				res.send('good!');
			}
		});
	});
});

router.use('/*', serveStatic('dist/admin.html', {'extensions': ['html']}));

//~~~~~~~~~~~~~~~
// first app endpoint <3
// (keep for posterity)
//~~~~~~~~~~~~~~~

app.get('/yo', function (req, res) {
    res.send('yo\n');
});

function calcPhotos(orderItems, orderBooks) {
	var totalPhotos = 192;
	var photosetSKU = app.get('products')['photoset'];
	for (var i = 0; i < orderItems.length; i++) {
		if (orderItems[i].parent == photosetSKU) {
			totalPhotos += ((orderItems[i].quantity / orderBooks) * 32);
		}
	}
	return totalPhotos;
}

function calcSoftcovers(orderItems) {
	var totalSoftcovers = 0;
	var softcoverSKU = app.get('products')['softcover'];
	orderItems.forEach(function(item) {
		if (item.parent == softcoverSKU) {
			totalSoftcovers = item.quantity;
		}
	});
	return totalSoftcovers;
}

function calcHardcovers(orderItems) {
	var totalHardcovers = 0;
	var hardcoverSKU = app.get('products')['hardcover'];
	orderItems.forEach(function(item) {
		if (item.parent == hardcoverSKU) {
			totalHardcovers = item.quantity;
		}
	});
	return totalHardcovers;
}

function sendOrderResponse(serverRes, orderID, cost, email, items, shipInfo, shipMethod, message, firebaseData) {
	db.ref('users').child(firebaseData.uid).child('events').child(firebaseData.eid).update({
		orderID: orderID
	});
	serverRes.send({
	    orderID: orderID,
	    cost: cost,
	    email: email,
	    items: items,
	    shippingInfo: shipInfo,
	    shippingMethod: shipMethod,
	    message: message
	}); 
	return;
}

function sendPromoResponse(serverRes, promoCode, promoValue, message) {
	serverRes.send({
		code: promoCode,
		value: promoValue,
		message: message
	});
	return;
}

function savePromoCodeToFirebase(uid, promoCode, promoValue, message) {
	db.ref('users').child(uid).child('cart_promo').set({
		code: promoCode,
		value: promoValue,
		message: message
	});
}

function saveCartToFirebase(uid, items, cost, promoCode) {
	console.log('saving to firebase:', items);
	db.ref('users').child(uid).child('cart').set(items);
	db.ref('users').child(uid).child('cart_subtotal').set(cost);
	db.ref('users').child(uid).child('cart_promo').set(promoCode);
}

function saveInProgressOrderToFirebase(uid, orderInfo) {
	db.ref('users').child(uid).child('inProgress').set(orderInfo);
}

function clearInProgressOrderFromFirebase(uid) {
	db.ref('users').child(uid).child('inProgress').set(false);
}

//~~~~~~~~~~~~~~~
// firebase + users
//~~~~~~~~~~~~~~~

/*

app.post('/verifyuser', function(req, res) {
    auth.createUser({
        email: email,
        emailVerified: verified,
        password: "secretPassword",
        displayName: "John Doe",
        photoURL: "http://www.example.com/12345678/photo.png",
        disabled: false
    }).then(function(userRecord) {
        console.log("Successfully created new user:", userRecord.uid);
    })
    .catch(function(error) {
        console.log("Error creating new user:", error);
    });
});
*/


function updateOrder(uid, email, orderID) {
}

app.post('/saveemail', function(req, res) {
	var email = req.body.email;
	var uid = req.body.uid;
	var f = req.body.first;
	var l = req.body.last;
	db.ref('emails').push({
		'email': email,
		'uid': uid,
		'firstName': f,
		'lastName': l
	});
	res.send('email saved!');
});

app.post('/temp', function(req, res) {
    var email = req.body.email;
    var temp = req.body.temp;
    var uid = req.body.uid;
    db.ref('temps').child(uid).update({
        token: temp
    });
    res.send('updated');
});


//~~~~~~~~~~~~~~~
// mailgun emails
//~~~~~~~~~~~~~~~

function composeVerifyEmail(e, c, r) {
	var token = jwt.sign({email: e, code: c, response: r}, app.get('jwtvar'), {});
	var message = 'Click here to verify your email and RSVP (or copy-paste this url into your browser):\n\nhttps://mydigitalguestbook.com/events.html?t=' + escape(token);
	return message;
}

function mkEventDir(uid, eid, urls, cover) {
	var eventDir = `./tmp/${eid}-`;
	console.log(urls);
	console.log(`event directory: ${eventDir}`)
	fs.mkdtemp(eventDir, function(err, folder) {
		if (err) { console.log(err); }
		downloadCover(folder, cover);
		for (var i = 0; i < urls.length; i++) {
			downloadImage(folder, urls[i], i, urls.length - 1, eid);
		}
	});
}

function downloadCover(tmpDir, url) {
	console.log(`cover url: ${url}`);
	var filename = `${tmpDir}/cover.jpg`;
	if (url.indexOf('stock_thumbs') >= 0) {
		url = `https://mydigitalguestbook.com/${url}`;
		url = url.replace('stock_thumbs', 'stock');
	}
	var writeFileStream = fs.createWriteStream(filename);
	request(url).pipe(writeFileStream).on('close', function(err) {
		if (err) throw err;
	});
}

function downloadImage(tmpDir, url, i, length, eid) {
	var filename = `${tmpDir}/img-${i}.jpg`;
	var writeFileStream = fs.createWriteStream(filename);
	const randoString = customString(6);
	var zipName = `${eid}-${randoString}.zip`;
	request(url).pipe(writeFileStream).on('close', function(err) {
		if (err) { console.log(err); return; }
		else {
			if (i === length) {
				setTimeout(function() {
					zipFolder(tmpDir, `../zip/${zipName}`, function(err) {
						if(err) { console.log('oh no! Zipping troubles', err); return; }
						fs.readFile(`../zip/${zipName}`, function (err, contents) {
							if (err) console.log(err);			
							dbx.filesUpload({ 
								path: `/events/${zipName}`, 
								contents: contents 
							}).then(function (response) {
								console.log(response);
								dbx.sharingAddFileMember({
									file: `/events/${zipName}`,
									members: [{email: 'emkolar@gmail.com'}, {email: 'technical@mydigitalguestbook.com'}]
								});
							}).catch(function (err) {
								console.log(err);
							});
						});
					});  
				}, 15000);
			}
		}
	});
}

function finalizeEventSubmission(uid, eid, res) {
	db.ref('users').child(uid).child('events').child(eid).child('finalized').set(true);
	res.send({ status: 'finalized' });
}

//~~~~~~~~~~~~~~~
// helpers
//~~~~~~~~~~~~~~~

function validatePromo(promo) {
	db.ref('coupons').once('value').then(function(snapshot) {
		var coupons = snapshot.value();
		if (coupons.hasOwnProperty(promo)) {
			return 'valid';
		}
		else {
			return 'invalid';
		}
	});
}

function getDateString(d) {
    return moment(d).format('MMMM Do, YYYY');
}

function customString(length) {
    let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (var i = length; i > 0; --i) {
        result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
}

function createUserEvent(uid, eventCode, summary, created, orderID, totalPhotos) {
    let eventKey = db.ref('users').child(uid).child('events').push().key;
    let webPage = 'https://mydigitalguestbook.com/events.html?id=' + escape(eventCode);
    let d = new Date();
    let dString = getDateString(d);
    let formattedD = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
    let event = {
        "coverImage" : "assets/stock_thumbs/default.png",
        "coverText" : {
            "line1" : {
                "text": "Custom Text Line1",
                "color": "#ffffff",
                "size": 24,
                "case": "mix"
            },
            "line2" : {
                "text": "Custom Text Line2",
                "color": "#ffffff",
                "size": 24,
                "case": "mix"
            },
            "line3" : {
                "text": "Custom Text Line3",
                "color": "#ffffff",
                "size": 24,
                "case": "mix"
            }
        },
        "created" : created,
        "dataURL" : " ",
        "dateString" : dString,
        "eventAddress1" : " ",
        "eventAddress2" : " ",
        "eventCity" : " ",
        "eventCode" : eventCode,
        "eventDate" : formattedD,
        "eventDetails" : " ",
        "eventID" : eventKey,
        "eventName" : "[Your New Event]",
        "eventPackage" : summary,
        "eventState" : " ",
        "eventURL" : webPage,
        "eventVenue" : " ",
        "eventZip" : " ",
        "guestDetails" : {
            "address" : true,
            "email" : true,
            "firstName" : true,
            "lastName" : true,
            "meal" : true,
            "partySize" : true,
            "registries" : true,
            "travel" : false,
            "phone": true
        },
        "locationInfo" : " ",
        "mealOptions" : {
            "beef" : false,
            "chicken" : false,
            "custom" : false,
            "description": false,
            "meals" : false,
            "pork" : false,
            "seafood" : false,
            "vegan" : false,
            "vegetarian" : false
        },
        "registries" : {
            "amazon" : false,
            "bbb" : false,
            "bestBuy" : false,
            "custom" : false,
            "kohls" : false,
            "macys" : false,
            "potteryBarn" : false,
            "stores" : false,
            "target" : false,
            "walmart" : false
        },
        "registryLinks" : {
	        "amazon" : " ",
            "bbb" : " ",
            "bestBuy" : " ",
            "custom" : " ",
            "kohls" : " ",
            "macys" : " ",
            "potteryBarn" : " ",
            "stores" : " ",
            "target" : " ",
            "walmart" : " "
        },
        "orderID": orderID,
        "photos": totalPhotos,
        "rsvpDeadline" : formattedD,
        "rsvps" : true,
        "thankYou" : {
            "line1" : "Thank you for responding!",
            "line2" : "We can't wait to see you on",
            "line3" : dString
        },
        "thumbs" : [],
        "tiny" : [],
        "urls" : [],
        "venueName" : "",
        "webImageURL" : "https://static.pexels.com/photos/68369/pexels-photo-68369.jpeg",
        "webText" : {
            "line1" : "Cordially Inviting You to Attend",
            "line2" : "[Your Event Name]",
            "line3" : "Send Acceptance or Regrets"
        }
    };
    let pagesObject = {
        eventID: eventKey,
        hostID: uid
    };
    db.ref('users').child(uid).child('events').child(eventKey).set(event);
    db.ref('events').child(uid).child(eventKey).set(event);
    db.ref('users').child(uid).child('books').child(eventKey).set({
	    coverImage: 'assets/stock_thumbs/default.png',
	    leftOut: [],
	    urls: []
    });
    db.ref('pages').child(eventCode).set(pagesObject);
}

//if (airbrake) { app.use(airbrake.expressHandler()); }

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);
