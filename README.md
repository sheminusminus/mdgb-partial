# some of the app files for mydigitalguestbook

this is some older code at this point, but still applies a bit.

there were three of us developing this application. i worked on the react code, node code, and also built the ios application.

the rest of this is the README file from the mdgb main repo:


## installation

### requirements

nodejs 6.9.x and npm.

### installing dependencies

global packages installation (run each of these):
```
npm install foundation-cli -g
npm install gulp-cli -g
npm install webpack -g
npm install bower -g
npm install nodemon -g
```

local packages installation:
```
npm run setup
```

## building and running the project

```
npm run dev
```

the site should then launch in your browser at localhost:8000. 

for the react app, click login or go to localhost:8000/admin.html.


## todo

- ~~submit button needs to save selections and trigger an email to MDGB~~

- remove all console.log() statements from clientside code

- ~~cart item titles are disappearing on mobile~~

- ~~copy-link feature is broken~~

- ~~alignment/positioning of text inputs on the guestbook and webpage tabs are jagged~~

- ~~app tips page needs content~~

- ~~toggling items on/off on guests tab has some ui weirdness (doesn't behave as expected)~~

- ~~need to show order history on account tab~~

- brand styling on guestbook cover titles

- ~~selecting a new event needs to highlight the event name in modal~~

- ~~make top-right login tab change from login to logout dynamically~~

- ~~create “wrong password” alert~~ (done but needs merge)

- create “wrong credit card information" message

- add a loader after account creation to smooth transition

- ~~autofocus next inputs in checkout process~~

- ~~moving backward in payment options erases all inputs~~

- ~~remove “sign me up” from landing page~~

- ~~rsvp results/reporting~~

- ~~communicate where those qr codes are~~

- ~~do not require address line 2 on payment form~~

- ~~tame down the frequency of verification emails being sent~~ (not us)

- ~~promo code needs to clear after purchase~~

- ~~add background generator to “wrong password” refresh~~

- ~~on the guestbook tab, the “edit text” button must be selected again to save the text in the database~~

- ~~revamp product displays in cart tab~~

- ~~update products to new pricing~~

- ~~fix photoviewer save buttons~~

- ~~remove save buttons from photoviewer, as they are no longer needed (autosaves)~~

- ~~redesign landing page~~ (needs photos + merge)
